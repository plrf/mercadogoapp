function completarcerosizquierda(as_cadena,ai_cantidad)
{
  li_n=ai_cantidad-as_cadena.length;
  li_i=0;
  ls_aux=as_cadena;
  for (li_i=1;li_i<=li_n;li_i++)
  {
    ls_aux="0"+ls_aux;
  }
  
  return ls_aux;
}



function replace(s,b,r)
{
  sigo=true;
  res=s;
  copia=res;
  res2=res;
  while (sigo)
  {
    sigo=true;
	//alert("res="+res);
	res2=res.replace(b,r,"gi");
	if (res2==copia)
	{
	  sigo=false;
	}
	else
	{
	  copia=res;
	  res=res2;
	  
	}
  }
  return res2;
}

function mensajeincluir(exitoso)
{
  if (exitoso=="si")		     
  {
	alert("Registro Incluido");	 
  }
  else if (exitoso=="error")		     
  {
    alert("Error al incluir");	 
  }
}


function mensajeincluir2(exitoso,codigo)
{
  aux=false;
  
  if (exitoso=="si")		     
  {
	alert("Registro Incluido "+codigo);	 
	aux=true;
  }
  else if (exitoso=="no")		     
  {
    alert("Error al incluir");	 
  }
  else if (exitoso=="error")		     
  {
    alert("Error al incluir");	 
  }
  
  return aux;
}


function mensajemodificar(exitoso)
{
  if (exitoso=="si")		     
  {
	alert("Registro actualizado");	 
  }
  else if (exitoso=="error")		     
  {
    alert("Error actualizando");	 
  }
  else
  {
	alert("No hubo cambios en el registro");	 	 
  }
}

function mensajemodificar2(exitoso)
{
  aux=false;
  if (exitoso=="si")		     
  {
	alert("Registro Actualizado");	 
	aux=true;
  }
  else if (exitoso=="error")		     
  {
    alert("Error al modificar");	 
  }
  else if (exitoso=="no")		     
  {
    alert("Error al modificar");	 
  }
  else
  {
	alert("No hubo cambios en el registro");	 	 
	aux=true;
  }
  return aux;
}


function mensajeeliminar(exitoso)
{
  if (exitoso=="si")		     
  {
	alert("Registro eliminado");	 
  }
  else if (exitoso=="error")		     
  {
	alert("Error eliminando el registro");	 
  }
  else
  {
    alert("No se eliminaron registros");	 	 
  }	
}

  

  function CeroSiBlanco(sn)
  {
    aux=sn;
    if (sn=="")	  
	{
	  aux="0";
	}
	return aux;
  }
  
  function codigoaleatorio()
  {
    var letras=new Array();
	letras[0]="A";
	letras[1]="a";
	letras[2]="B";
	letras[3]="b";
	letras[4]="C";
	letras[5]="c";
	letras[6]="D";
	letras[7]="d";
	letras[8]="E";
	letras[9]="e";
	letras[10]="F";
	letras[11]="f";
	letras[12]="G";
	letras[13]="g";
	letras[14]="H";
	letras[15]="h";
	
	res="";
	for (i=0;i<100;i++)
	{
	  var n=Math.round(Math.random()*15);
	  res=res+letras[n];
	}
	
     return res;
  }

  function getFecha()
  {
	var fecha=new Date();  
	
	dia=fecha.getDate();
	mes=fecha.getMonth()+1;//empieza en cero
	ano=fecha.getFullYear();
	
	sdia=completarcerosizquierda(""+dia,2);
	smes=completarcerosizquierda(""+mes,2);
	sfecha=sdia+"/"+smes+"/"+ano;
	
	return sfecha;
  }
  
  function invertirfecha(sf)
  {
	sfdia=sf.substring(0,2);  
	sfmes=sf.substring(3,5);  
	sfano=sf.substring(6,10);  
	
	sfres=sfano+sfmes+sfdia;
	return sfres;
  }
  
  function compararfechas(f1,f2)  
  {
    comfecha=0;
	
	if1=invertirfecha(f1);
	if2=invertirfecha(f2);
	
	if (if1>if2)
	{
	  comfecha=1;	
	}
	else if (if1<if2)
	{
	  comfecha=-1;		
	}
	else
	{
	  comfecha=0;		
	}
	
	return comfecha;
  }
  
  function redondeo(num,numDec)
  {
    num = num * Math.pow(10,numDec)
    num = Math.round(num)
    num = num / Math.pow(10,numDec)
    return num
  }
  
  function cadenaaleatoria(longitud)
  {
    var letras=new Array();
	letras[0]="A";
	letras[1]="a";
	letras[2]="B";
	letras[3]="b";
	letras[4]="C";
	letras[5]="c";
	letras[6]="D";
	letras[7]="d";
	letras[8]="E";
	letras[9]="e";
	letras[10]="F";
	letras[11]="f";
	letras[12]="G";
	letras[13]="g";
	letras[14]="H";
	letras[15]="h";
	
	res="";
	for (i=0;i<longitud;i++)
	{
	  var n=Math.round(Math.random()*15);
	  res=res+letras[n];
	}
	
    return res;
  }
  
  function limpiarnumero(snum)
  {
	aux=replace(snum,".","");  
	aux=replace(aux,",",".");  
	return aux;
  }
  
  function aNumero(nn)
  {	
    naux=0;
	if (nn!="")  
	{
	  naux=parseFloat(limpiarnumero(nn));	
    }
	return naux;
  }
  
  function formatearnumero(nnn,ndecimal)
  {
	nn=""+nnn;  
	p=nn.indexOf(".");  
	entero=nn;
	decimal="";
	if (p>=0)
	{
	  entero=nn.substring(0,p);
	  decimal=nn.substring(p+1,nn.length);	  
	  if (ndecimal<decimal.length)
	  {
		decimal=decimal.substring(0,ndecimal);  
	  }
	}	
	
	decimal=completarcerosderecha(decimal,ndecimal);
	
	nlongitud=entero.length;
	fentero="";
	while ((entero.length>3))
	{
	  aux=entero.substring(entero.length-3,entero.length);	
	  if (fentero!="")
	  {
		fentero=aux+"."+fentero;  
	  }
	  else
	  {
		fentero=aux;  
	  }
	  entero=entero.substring(0,entero.length-3);
	}
	if (entero!="")
	{
	  if (fentero!="")	
	  {
	    fentero=entero+"."+fentero;	
	  }
	  else
	  {
		fentero=entero;	  
	  }
	}
	res=fentero;
	if (ndecimal>0)
	{
	  if (decimal!="")	
	  {
		res=res+","+decimal;  
	  }
	}
    return res;
  }
  
  function formatearnumeroing(nnn,ndecimal)
  {
	nn=""+nnn;  
	p=nn.indexOf(".");  
	entero=nn;
	decimal="";
	if (p>=0)
	{
	  entero=nn.substring(0,p);
	  decimal=nn.substring(p+1,nn.length);	  
	  if (ndecimal<decimal.length)
	  {
		decimal=decimal.substring(0,ndecimal);  
	  }
	}	
	
	decimal=completarcerosderecha(decimal,ndecimal);
	
	nlongitud=entero.length;
	fentero="";
	while ((entero.length>3))
	{
	  aux=entero.substring(entero.length-3,entero.length);	
	  if (fentero!="")
	  {
		fentero=aux+","+fentero;  
	  }
	  else
	  {
		fentero=aux;  
	  }
	  entero=entero.substring(0,entero.length-3);
	}
	if (entero!="")
	{
	  if (fentero!="")	
	  {
	    fentero=entero+"."+fentero;	
	  }
	  else
	  {
		fentero=entero;	  
	  }
	}
	res=fentero;
	if (ndecimal>0)
	{
	  if (decimal!="")	
	  {
		res=res+","+decimal;  
	  }
	}
    return res;
  }

function completarcerosderecha(as_cadena,ai_cantidad)
{
  li_n=ai_cantidad-as_cadena.length;
  li_i=0;
  ls_aux=as_cadena;
  for (li_i=1;li_i<=li_n;li_i++)
  {
    ls_aux=ls_aux+"0";
  }
  
  return ls_aux;
}


function getParameterByName(name, url)
  {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}