
   var http_requestnotificar = false;
   function makePOSTRequestnotificar(url, parameters) {
      http_requestnotificar = false;
      if (window.XMLHttpRequest) {
		   
         http_requestnotificar = new XMLHttpRequest();
		  
         if (http_requestnotificar.overrideMimeType) {
         	// set type accordingly to anticipated content type
			
            http_requestnotificar.overrideMimeType('text/xml');
			
            //http_requestnotificar.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_requestnotificar = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_requestnotificar = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_requestnotificar) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }

      http_requestnotificar.onreadystatechange = respuestaServidornotificacion;
      http_requestnotificar.open('POST', url, true);
      http_requestnotificar.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_requestnotificar.setRequestHeader("Content-length", parameters.length);
      http_requestnotificar.setRequestHeader("Connection", "close");
      http_requestnotificar.send(parameters);

   }
   
   function iralservidornotificacion(pagina,objetos,nobjetos)
   {
     var f=document.form1;
	 var poststr = "";
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
      
      makePOSTRequestnotificar(pagina, poststr);
   }
   
   
   function iralservidorextnotificacion(pagina,poststr,objetos,nobjetos)
   {
     var f=document.form1;	 
	 for (i=0;i<nobjetos;i++)
	 {
	  // alert("i="+i);
	   alert("obj="+objetos[i]);
       if (poststr=="") 	 
	   {
	     poststr=objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
      
      makePOSTRequestnotificar(pagina, poststr);
   }
   
   function iralservidor2notificacion(pagina,objetos,nobjetos,postext)
   {
     var f=document.form1;
	 var poststr = "";
	 var i=0;
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("i="+i+"obj="+objetos[i]+" "+f.elements[objetos[i]].value);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
     poststr+=postext; 	 
	 
     makePOSTRequestnotificar(pagina, poststr);
   }
   
   
   function iralservidor3notificacion(pagina,objetos,nobjetos,postext,postext2)
   {
     var f=document.form1;
	 var poststr = "";
	 
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("i="+i+"obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
     poststr+=postext; 
	 
	 
     poststr+=postext2; 
	 
	 
	  
     makePOSTRequestnotificar(pagina, poststr);
   }


  function decodenotificacion(str)
  {
    return unescape(str.replace(/\+/g, " "));
  }
  
  function limpiarvaciosnotificacion(s)
  {
    s=s.replace(/@@--@@/g,"&");   
	return s.replace(/@@-@@/g,"");   
  }
  
  function limpiarhtmlnotificacion(s)
  {
    s=s.replace(/@@mayorque@@/g,">");   
	return s.replace(/@@menorque@@/g,"<");   
  }
