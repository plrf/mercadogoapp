var app = {

    findByName: function() {
        console.log('findByName');
        this.store.findByName($('.search-key').val(), function(employees) {
            var l = employees.length;
            var e;
            $('.employee-list').empty();
            for (var i=0; i<l; i++) {
                e = employees[i];
                $('.employee-list').append('<li><a href="#employees/' + e.id + '">' + e.firstName + ' ' + e.lastName + '</a></li>');
            }
        });
    },
	
  cargado: function()
  {
    f=document.form1;
    f.txtoperacion.value="cargar galeria";
	f.txtcodtippro.value="-";
	f.txtcodmarfunpro.value="-";
	f.txtcodmodfunpro.value="-";
	f.txtanomod.value="-";
	f.txtndesreg.value="0";
	
	//this.ejecutarcargargaleria();
	alert("Iniciando APP");
	
  },
  
  
  http_request:false,
  
  makePOSTRequest: function(url, parameters)
   {
      this.http_request = false;
      if (window.XMLHttpRequest) {
		   
         this.http_request = new XMLHttpRequest();
		  
         if (this.http_request.overrideMimeType) {
         	// set type accordingly to anticipated content type
			
            this.http_request.overrideMimeType('text/xml');
			
            //http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            this.http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               this.http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!this.http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }

      this.http_request.onreadystatechange = respuestaServidor;
      this.http_request.open('POST', url, true);
      this.http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      this.http_request.setRequestHeader("Content-length", parameters.length);
      this.http_request.setRequestHeader("Connection", "close");
      this.http_request.send(parameters);

   },
   
   
  
   
   
   iralservidor: function(pagina,objetos,nobjetos)
   {
     f=document.form1;
	 var poststr = "";
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
      
      this.makePOSTRequest(pagina, poststr);
   },
   
   ejecutarcargargaleria: function ()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodtippro";
	obj[2]="txtcodmarfunpro";
	obj[3]="txtcodmodfunpro";
	obj[4]="txtanomod";
	obj[5]="txtndesreg";
	obj[6]="txtslg";
	
    this.iralservidor("http://www.ecotechne.com.ve/ecotechneweb/cms/svcnproductosfuncionalesapp.php",obj,7);
  },

  
  bindEvents: function() {
        
        document.addEventListener('deviceready', this.onDeviceReady, false);
	    
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
      
	RegisterWithFCM();
	//Despues de registrarse, puedo esperar notificaciones
	GetMessageFromFCM();
	
	document.addEventListener("backbutton", ejecutarvolver, true);	
	
	//window.open = cordova.InAppBrowser.open; //28052020
	
	/*cordova.plugins.backgroundMode.setDefaults({
title: "MercadoGO",
text: "MercadoGO en ejecución",
icon: 'icon', // this will look for icon.png in platforms/android/res/drawable|mipmap
color: "6E10A0", // hex format like 'F14F4D'
resume: false,
silent: false,
hidden: true, //false
bigText: false,
});
	
	cordova.plugins.backgroundMode.enable();
	
	cordova.plugins.backgroundMode.on('activate', function()
 	{
      cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
    });*/
	
	},
  
    initialize: function() {
        /*this.store = new MemoryStore();*/
        /*$('.search-key').on('keyup', $.proxy(this.findByName, this));*/
		//$.support.cors = true; 28052020
		//this.cargado();
		//alert("Esto ocurre");
      this.bindEvents();
	  
	  
	}

};

app.initialize();

  
  function respuestaServidor()
   {
     f=document.form1;
	 http_request=app.http_request;
     if (http_request.readyState == 4)
	 {
       if (http_request.status == 200)
	   {
         //alert(http_request.responseText);
		 //f.txterror.value=http_request.responseText;
			
	     if (http_request.responseText.indexOf('@@@incorrecto@@@') == -1)
		 { 
           var xmlDocument = http_request.responseXML; 
		   
		   var operacion = xmlDocument.getElementsByTagName('operacion').item(0).firstChild.data;
		   if (operacion=="cargar galeria")
		   {
			 f=document.form1; 
			 
			 var htmlescritorio = xmlDocument.getElementsByTagName('htmlescritorio').item(0).textContent;   			 
			 htmlescritorio=limpiarhtml(htmlescritorio);
			 
			 htmlescritorio=limpiarvacios(htmlescritorio);
			 var htmlmovil = xmlDocument.getElementsByTagName('htmlmovil').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 
			 var objdivcapagaleriaproductos=document.getElementById("divcapagaleriaproductos");
		     objdivcapagaleriaproductos.innerHTML=htmlescritorio;
			 
			 
			
		   }
		   
		   
           isWorking = false;
        }
	    else
	    {
          alert('Hay un problema con la solicitud de datos');
        }
	  }
    }
  }
  
  