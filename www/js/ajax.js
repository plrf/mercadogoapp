
   var http_request = false;
   function makePOSTRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) {
		   
         http_request = new XMLHttpRequest();
		  
         if (http_request.overrideMimeType) {
         	// set type accordingly to anticipated content type
			
            http_request.overrideMimeType('text/xml');
			
            //http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }

      http_request.onreadystatechange = respuestaServidor;
      http_request.open('POST', url, true);
      http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request.setRequestHeader("Content-length", parameters.length);
      http_request.setRequestHeader("Connection", "close");
      http_request.send(parameters);

   }
   
   function iralservidor(pagina,objetos,nobjetos)
   {
     f=document.form1;
	 var poststr = "";
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("i="+i+"obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
      
      makePOSTRequest(pagina, poststr);
   }
   
   
   function iralservidorext(pagina,poststr,objetos,nobjetos)
   {
     f=document.form1;	 
	 for (i=0;i<nobjetos;i++)
	 {
	  // alert("i="+i);
	  // alert("obj="+objetos[i]);
       if (poststr=="") 	 
	   {
	     poststr=objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURIComponent( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
      
      makePOSTRequest(pagina, poststr);
   }
   
   function iralservidor2(pagina,objetos,nobjetos,postext)
   {
     f=document.form1;
	 var poststr = "";
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("i="+i+"obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
     poststr+=postext; 
	  
     makePOSTRequest(pagina, poststr);
   }
   
   
   function iralservidor3(pagina,objetos,nobjetos,postext,postext2)
   {
     f=document.form1;
	 var poststr = "";
	 
	 for (i=0;i<nobjetos;i++)
	 {
	   //alert("i="+i);
	   //alert("i="+i+"obj="+objetos[i]);
       if (i==0) 	 
	   {
	     poststr=objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );
	   }
	   else
	   {
         poststr+="&"+objetos[i]+"="+encodeURI( document.getElementById(objetos[i]).value );	   
	   }	 
	 }
     poststr+=postext; 
	 
	 
     poststr+=postext2; 
	 
	 
	  
     makePOSTRequest(pagina, poststr);
   }


  function decode(str)
  {
    return unescape(str.replace(/\+/g, " "));
  }
  
  function limpiarvacios(s)
  {
    s=s.replace(/@@--@@/g,"&");   
	return s.replace(/@@-@@/g,"");   
  }
  
  function limpiarhtml(s)
  {
    s=s.replace(/@@mayorque@@/g,">");   
	return s.replace(/@@menorque@@/g,"<");   
  }
