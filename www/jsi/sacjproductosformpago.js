function obtenerfocomonsalcre()
  {
    var obj=document.getElementById("txtmonsalcre");
    var valor=obj.value;	
	var svalor=aNumero(valor);
	obj.value=svalor;
  }
  
  function perderfocomonsalcre()
  {
    var obj=document.getElementById("txtmonsalcre");
    if (validarreal(obj))
	{
	  var valor=obj.value;	
	  var svalor=formatearnumero(valor,2);
	  obj.value=svalor;
	
	  f=document.form1;
	}
	else
	{
	  obj.value=formatearnumero("0",2);	
	}	
  }
 

function obtenerfocomonpag(nmonpag)
{
   var obj=document.getElementById("txtmonpago"+nmonpag);
   var valor=obj.value;	
   var svalor=aNumero(valor);
   obj.value=svalor;
}
  
  function perderfocomonpag(nmonpag)
  {
    var obj=document.getElementById("txtmonpago"+nmonpag);
    if (validarreal(obj))
	{
	  var valor=obj.value;	
	  var svalor=formatearnumero(valor,2);
	  obj.value=svalor;
	
	  f=document.form1;
	}
	else
	{
	  obj.value=formatearnumero("0",2);	
	}
	calculartotal();
	
	
  }
  
  function calculardiferenciaefectivovuelto()
  {
    f=document.form1;
	//var valor=aNumero(f.txtmontoapagar.value);
	var valor=aNumero(f.txtmontototal.value);
	var n1=valor;
	
	
	valor2=f.txtmonpagefe.value;
	var n2=0;
	
	if (valor2!="")
	{	  
	  n2=aNumero(valor2);
	  //n2=parseFloat(valor2);
	  
	}
	
	
	var npago=n1;
	var n3=redondeo(n2-npago,2);
	if (n3>0)
	{
	  var sp=formatearnumero(n3,2);
	  var objd=document.getElementById("divdiferenciavuelto");
	  objd.innerHTML=sp;
	}
	else
	{
	  //alert("Su pago en efectivo debe ser mayor o igual al monto a pagar");
	  var sp="0,00";
	  var objd=document.getElementById("divdiferenciavuelto");
	  objd.innerHTML=sp;
	}
  }
  
  function calculardiferencia()
  {
    f=document.form1;
	var valor=aNumero(f.txtmontototal.value);
	var n1=valor;
	
	valor2=f.txtmontoapagar.value;
	var n2=0;
	
	if (valor2!="")
	{	  
	  //n2=aNumero(valor2);
	  n2=parseFloat(valor2);
	  
	}
	
	
	var npago=n1;
	var n3=redondeo(n2-npago,2);
	var sp=formatearnumero(n3,2);
	var objd=document.getElementById("divdiferenciapagar");
	objd.innerHTML=sp;
  }
  
  function calculartotal()
  {
    var valido=true;
	f=document.form1;
	var stotal=f.txtmontoapagar.value;
	var ntotal=aNumero(stotal);
	var scantraban=f.txtcantraban.value;	
	var ncantraban=parseFloat(scantraban);
	var sumador=0;
	var i=0;
	
	
	for (i=0;i<ncantraban;i++)
	{
	  var monpago=f.elements["txtmonpago"+i].value;
	  var nmonpago=aNumero(monpago);
	  sumador=redondeo(sumador+nmonpago,2);
	  
	}
	var nmonbon=aNumero(f.txtmonbon.value);
	sumador=redondeo(sumador+nmonbon,2);
	
	var ssumador=formatearnumero(sumador,2);
	
	f.txtmontototal.value=ssumador;
	calculardiferencia();
	return valido;
  }
  
  function mascantraban()
  {
    
	f=document.form1;
	var nfilas=parseInt(f.txtnfiltraban.value);
	
	var ncantraban=parseFloat(f.txtcantraban.value);
	if (ncantraban<nfilas)
	{
	  ncantraban++;
	}
	f.txtcantraban.value=ncantraban;
	document.getElementById("divcantraban").innerHTML=ncantraban;
	mostrarfilaspago();
  }

  function menoscantraban()
  {
    f=document.form1;
	var nfilas=parseInt(f.txtnfiltraban.value);
	
	var ncantraban=parseFloat(f.txtcantraban.value);
	if (ncantraban>0)
	{
	  ncantraban--;
	}
	f.txtcantraban.value=ncantraban;
	document.getElementById("divcantraban").innerHTML=ncantraban;
    mostrarfilaspago();
  }
  
  function mostrarfilaspago()
  {
    f=document.form1;
	var nfilas=parseInt(f.txtnfiltraban.value);
	
	var ncantraban=parseFloat(f.txtcantraban.value);
	var i=0;
	for (i=0;i<nfilas;i++)
	{
	  var b="none";
	  if (i<ncantraban)
	  {
	    b="block";
	  }
	  var obj=document.getElementById("divfiladetallepago"+i);
	  obj.style.display=b;
	}
	calculartotal();
  }
  
  function cambiocodciu()
  {
    f=document.form1;
	if (f.cmbcodciuenv.value!="-")
	{
	  f.txtcodurbenv.value="";
  	  f.txtoperacion.value="cambio codciu";
	  f.txtcodciuenv.value=f.cmbcodciuenv.value;
	  ejecutarcodciuenv();
	}
	else
	{
	  limpiarcodurbenv();
	} 
  }

  function cambiocodciuconsultar()
  {
    f=document.form1;
	if (f.cmbcodciuenv.value!="-")
	{
	  //f.txtcodurbenv.value="";
  	  f.txtoperacion.value="cambio codciu";
	  f.txtcodciuenv.value=f.cmbcodciuenv.value;
	  ejecutarcodciuenv();
	}
	else
	{
	  limpiarcodurbenv();
	} 
  }  
  
  function limpiarcodurbenv()
  {
	if (f.cmbcodurbenv.options)//limpiando el combo
	{
	  i=0;
      for (m=f.cmbcodurbenv.options.length;m>0;m--)
         f.cmbcodurbenv.remove(i);
	}
	
	var opcion=document.createElement("option");
    opcion.value="-";
    opcion.text="Seleccione Uno";
	f.cmbcodurbenv.options.add(opcion);   
  }

  function paraenviargrid()
   {
     f=document.form1;	 
	 var poststr = "";
	 var nfilas=parseInt(f.txtnfiltraban.value);
	 totalfilas=nfilas;
	 var objetos=new Array();	 
	 objetos[0]="cmbctapag";	 
	 objetos[1]="txtmonpago";	 
	 objetos[2]="txtrefpag";	 
	 objetos[3]="txtfecref";
     objetos[4]="txtcodpag";	 
	 
	 for (i=0;i<totalfilas;i++)
	 {    
	   for (j=0;j<5;j++)   	   
	   {	     
         poststr+="&"+objetos[j]+i+"="+encodeURI( f.elements[objetos[j]+i].value );	   	   
       }		 
	 }    
	 
	 return poststr;       
   }
   
   function continueship()
   {
     f=document.form1;
	 f.txtfecent.value=f.cmbfecent.value;
	 f.txtstaenvexp.value="N";
	 var objhe=document.getElementById("divcapahoraexpress");
	 if (objhe!=null)
	 {
	   if (objhe.style.display=="block")
	   {
	     f.txtfecent.value=f.txtfecenthoy.value;
	     f.txthorent.value=f.cmbhorhorexp.value+":"+f.cmbminhorexp.value;
	     f.txtstaenvexp.value="S";  
         //alert(f.txthorent.value);
		
	   }
	 }
	 if (camposvalidos())
	 {
	   //f.action="saciverifybeforesend.php";
	   //f.submit();
	   //mostrarmodalcargando();
	   f.txtoperacion.value="incluir vtaapp";
	   ejecutar();
	 }
   }
   
   function continueshiptc()
   {
     f=document.form1;
	 f.txtfecent.value=f.cmbfecent.value;
	 f.txtstaenvexp.value="N";
	 var objhe=document.getElementById("divcapahoraexpress");
	 if (objhe!=null)
	 {
	   if (objhe.style.display=="block")
	   {
	     f.txtfecent.value=f.txtfecenthoy.value;
	     f.txthorent.value=f.cmbhorhorexp.value+":"+f.cmbminhorexp.value;
	     f.txtstaenvexp.value="S";  
         //alert(f.txthorent.value);
		
	   }
	 }
	 if (camposvalidos())
	 {
	   //f.action="saciverifybeforesend.php";
	   //f.submit();
	   //mostrarmodalcargando();
	   f.txtoperacion.value="incluir vtaapp";
	   ejecutar();
	 }
   }
   
   function continueshipefectivo()
   {
     f=document.form1;
	 f.txtfecent.value=f.cmbfecent.value;
	 f.txtstaenvexp.value="N";
	 var objhe=document.getElementById("divcapahoraexpress");
	 if (objhe!=null)
	 {
	   if (objhe.style.display=="block")
	   {
	     f.txtfecent.value=f.txtfecenthoy.value;
	     f.txthorent.value=f.cmbhorhorexp.value+":"+f.cmbminhorexp.value;
	     f.txtstaenvexp.value="S";  
         //alert(f.txthorent.value);
		
	   }
	 }
	 if (camposvalidos())
	 {
	   //f.action="saciverifybeforesend.php";
	   //f.submit();
	   mostrarmodalcargando();
	   f.txtoperacion.value="incluir vtaapp";
	   ejecutar();
	 }
   }
  
  function enviovalido()
  {
    var valido=false;
	
	if (f.txtfulnamenv.value=="")
	{
	  alert("El nombre de quien recibe no puede estar en blanco");
	  f.txtfulnamenv.focus();
	}
	/*else if (f.txtcedide.value=="")
	{
	  alert("La cedula de quien recibe no puede estar en blanco");
	  f.txtcedide.focus();
	}*/
	else if (f.txttelusuenv.value=="")
	{
	  alert("El telefono de quien recibe no puede estar en blanco");
	  f.txttelusuenv.focus();
	}
	else if (f.cmbcodpaienv.value=="-")
	{
	  alert("Debe seleccionar el pais de quien recibe");
	  f.cmbcodpaienv.focus();
	}
	else if (f.cmbcodestenv.value=="-")
	{
	  alert("Debe seleccionar el estado de quien recibe");
	  f.cmbcodestenv.focus();
	}
	else if (f.cmbcodciuenv.value=="-")
	{
	  alert("Debe seleccionar la ciudad de quien recibe");
	  f.cmbcodciuenv.focus();
	}
	else if (f.cmbcodurbenv.value=="-")
	{
	  alert("Debe seleccionar la urbanización de quien recibe");
	  f.cmbcodurbenv.focus();
	}
	else if (f.txtdirusuenv.value=="")
	{
	  alert("La dirección de quien recibe no puede estar en blanco");
	  f.txtdirusuenv.focus();
	}
	else if (f.txthorent.value=="")
	{
	  alert("Debe seleccionar el bloque de hora de entrega");
	}
	else
	{
	  valido=true;
	}
	return valido;
  }
  
  function camposvalidos()
  {
    var valido=false;
    f=document.form1;
	
	//f.txthorent.value=f.cmbhorhorent.value+":"+f.cmbminhorent.value;
	if (!pagovalido())
	{
	  
	}
	else if (f.txtfulnamenv.value=="")
	{
	  alert("El nombre de quien recibe no puede estar en blanco");
	  f.txtfulnamenv.focus();
	}
	/*else if (f.txtcedide.value=="")
	{
	  alert("La cedula de quien recibe no puede estar en blanco");
	  f.txtcedide.focus();
	}*/
	else if (f.txttelusuenv.value=="")
	{
	  alert("El telefono de quien recibe no puede estar en blanco");
	  f.txttelusuenv.focus();
	}
	else if (f.cmbcodpaienv.value=="-")
	{
	  alert("Debe seleccionar el pais de quien recibe");
	  f.cmbcodpaienv.focus();
	}
	else if (f.cmbcodestenv.value=="-")
	{
	  alert("Debe seleccionar el estado de quien recibe");
	  f.cmbcodestenv.focus();
	}
	else if (f.cmbcodciuenv.value=="-")
	{
	  alert("Debe seleccionar la ciudad de quien recibe");
	  f.cmbcodciuenv.focus();
	}
	else if (f.cmbcodurbenv.value=="-")
	{
	  alert("Debe seleccionar la urbanización de quien recibe");
	  f.cmbcodurbenv.focus();
	}
	else if (f.txtdirusuenv.value=="")
	{
	  alert("La dirección de quien recibe no puede estar en blanco");
	  f.txtdirusuenv.focus();
	}
	else if (f.txthorent.value=="")
	{
	  alert("Debe seleccionar el bloque de hora de entrega");
	}
	else
	{
	  valido=true;
	}
	return valido;
  }
  
  function pagovalidotb()
  {
    var valido=true;
	f=document.form1;
	var stotal=f.txtmontoapagar.value;
	var ntotal=parseFloat(stotal);
	
	var objvalido=document.getElementById("divdiferenciapagar");
	var npagod=aNumero(objvalido.innerHTML);
	
	var stotal2=f.txtmontototal.value;
	var nmontototal=aNumero(stotal2);	
	
	var scantraban=f.txtcantraban.value;	
	var ncantraban=parseFloat(scantraban);
	var fechahoy=f.txtfechahoy.value;
	
	var i=0;
	while ((i<ncantraban)&&(valido))
	{
	  var ctapag=f.elements["cmbctapag"+i].value;
	  
	  var monpago=f.elements["txtmonpago"+i].value;
	  var refpag=f.elements["txtrefpag"+i].value;
	  var dia=f.elements["cmbdiafecref"+i].value;
	  var mes=f.elements["cmbmesfecref"+i].value;
	  var ano=f.elements["cmbanofecref"+i].value;
	  var fecha=dia+"/"+mes+"/"+ano;
	  f.elements["txtfecref"+i].value=fecha;
	  if (ctapag=="-")
	  {
	    alert("Debe seleccionar el numero de cuenta");
		f.elements["cmbctapag"+i].focus();
		valido=false;
	  }
	  else if (monpago=="")
	  {
	    alert("Debe introducir el monto del pago");
		f.elements["txtmonpago"+i].focus();
		valido=false;
	  }
	  else if (refpag=="")
	  {
	    alert("Debe introducir el numero de transferencia");
		f.elements["txtrefpag"+i].focus();
		valido=false;
	  }
	  else if (!fechavalida(dia,mes,ano))
	  {
	    alert("Debe introducir una fecha valida");
		f.elements["cmbdiafecref"+i].focus();
		valido=false;
	  }
	  else if (!validarrangofechas(fecha,fechahoy))
	  {
	    alert("Fecha de transferencia incorrecta");
		f.elements["cmbdiafecref"+i].focus();
		valido=false;
	  }
	  i++;
	}
	
	var nmonto1=formatearnumero(""+nmontototal,2);
	var nmonto2=formatearnumero(""+ntotal,2);
	//alert(""+nmonto1);
	//alert(""+nmonto2);
	//if (nmontototal!=ntotal)
	var objvalido=document.getElementById("divdiferenciapagar");
	var npago=aNumero(objvalido.innerHTML);
	
	//if (nmonto1!=nmonto2)
	if (npago!=0)
	{
	  valido=false;
	  alert("El monto total de las transferencias no coincide con el total de la compra");
	}
	
	return valido;
  }
  
  function pagovalidotc()
  {
    var valido=true;
	f=document.form1;
	var stotal=f.txtmontoapagar.value;
	var ntotal=parseFloat(stotal);
	
	
	return valido;
  }

  function pagovalidoef()
  {
    var valido=false;
	f=document.form1;
	if (f.txtmonpagefe.value!="")
	{
	  //var valor=aNumero(f.txtmontoapagar.value);
	  var valor=aNumero(f.txtmontototal.value);
	  var n1=valor;
	
	  valor2=f.txtmonpagefe.value;
	  var n2=0;
	
	  if (valor2!="")
	  {	  
	    n2=aNumero(valor2);	    
	  }
	  
	  if (n2>=n1)
	  {
	    valido=true;
	  }
	  else
	  {
	    alert("Su pago en efectivo debe ser mayor o igual al monto a pagar");
	  }
	}
	else
	{
	  alert("El monto de su pago en efectivo no puede estar en blanco");
	}
	
	return valido;
  }
  
  function pagovalido()
  {
    var valido=false;
	
	f=document.form1;
	//alert("fp="+f.cmbformapago.value);
	if (f.cmbformapago.value=="-")
	{
	  alert("Debe seleccionar la forma de pago");
	}
	else if (f.cmbformapago.value=="TB")
	{
	  valido=pagovalidotb();
	}
	else if (f.cmbformapago.value=="PP")
	{
	  valido=pagovalidotc();
	}
	else if (f.cmbformapago.value=="EF")
	{
	  valido=pagovalidoef();
	}
	
	return valido;
  }
  
  function seleccionarbloquehora(bloque,nb)
  {
    f=document.form1;
	var i=0;
	var cant=f.txtcantbloquehoras.value;
	var ncant=parseInt(cant);
	for (i=0;i<ncant;i++)
	{
      var obj=document.getElementById("botselbloque"+i);
	  obj.className="enlacebotonazul60px";
	}
	var obj=document.getElementById("botselbloque"+nb);
	obj.className="enlaceboton2azul60px";
	f.txthorent.value=bloque;
  }
  
  function seleccionarbloquehoraman(bloque,nb)
  {
    f=document.form1;
	var i=0;
	var cant=f.txtcantbloquehorasman.value;
	var ncant=parseInt(cant);
	for (i=0;i<ncant;i++)
	{
      var obj=document.getElementById("botselbloqueman"+i);
	  obj.className="enlacebotonazul60px";
	}
	var obj=document.getElementById("botselbloqueman"+nb);
	obj.className="enlaceboton2azul60px";
	f.txthorent.value=bloque;
  }
  
  function cambiofecent()
  {
    var obj1=document.getElementById("capahorashoy");
	var obj2=document.getElementById("capahorasmanana");
	f=document.form1;
	f.txthorent.vaue="";
	//desmarcando horas
	var cant=f.txtcantbloquehoras.value;
	var ncant=parseInt(cant);
	for (i=0;i<ncant;i++)
	{
      var obj=document.getElementById("botselbloque"+i);
	  obj.className="enlacebotonazul60px";
	}
	
	var i=0;
	var cant=f.txtcantbloquehorasman.value;
	var ncant=parseInt(cant);
	for (i=0;i<ncant;i++)
	{
      var obj=document.getElementById("botselbloqueman"+i);
	  obj.className="enlacebotonazul60px";
	}
	
	
	if (f.cmbfecent.selectedIndex==0)
	{
	  //hoy
	  obj2.style.display="none";
	  obj1.style.display="block";	  
	  var obj3=document.getElementById("divcapaenvioexpress");
	  obj3.style.display="block";
	  seleccionarenvioexpressvolver();
	}
	else
	{
	  //manana
	  obj1.style.display="none";
	  obj2.style.display="block";
	  var obj3=document.getElementById("divcapaenvioexpress");
	  obj3.style.display="none";
	  var objhoras=document.getElementById("divcapahoraexpress");
	  objhoras.style.display="none";
	}
  }
  
  function seleccionarenvioexpress()
  {
    var obj=document.getElementById("divcapabotonenvioexpress");
	var obj2=document.getElementById("divcapabotonenvioexpressvolver");
	var objhoras=document.getElementById("divcapahoraexpress");
	var objhorasentrega=document.getElementById("capahorashoy");
	
	obj.style.display="none";
	objhorasentrega.style.display="none";
	obj2.style.display="block";
	objhoras.style.display="block";
	
	f=document.form1;
	
	//colocar los montos visibles
	var ncosenvexp="0";
	if (f.txttipmon.value=="bss")
	{
	  ncosenvexp=f.txtcosenvexpbss.value;
	}
	else
	{
	  ncosenvexp=f.txtcosenvexp.value;
	}
	var obj11=document.getElementById("divcosenvexp");
	obj11.innerHTML=f.txtscosenvexp.value;
	//calcular el total
	calculartotaladicionales(ncosenvexp);
  }
  
  function calculartotaladicionales(cee)
  {
    f=document.form1;
	var sn1=f.txtmontoapagaroriginal.value;
	var sn2=cee;
	
	var n1=parseFloat(sn1);
	var n2=parseFloat(sn2);
	
	var n3=redondeo(n1+n2,3);
	var sn3=formatearnumero(n3,2);
	f.txtmontoapagar.value=n3;
	
	var obj4=document.getElementById("divtotalapagar");
	obj4.innerHTML=sn3;
	
	var sn4=formatearnumero(sn2,2);
	var obj4=document.getElementById("divcosenvexp");
	obj4.innerHTML=sn4;
	
	//f.txtmonpago.value=sn3;
	
	f.elements["txtmonpago0"].value=sn3;
	
  }
  
  function seleccionarenvioexpressvolver()
  {
    var obj=document.getElementById("divcapabotonenvioexpress");
	var obj2=document.getElementById("divcapabotonenvioexpressvolver");
	var objhoras=document.getElementById("divcapahoraexpress");
	var objhorasentrega=document.getElementById("capahorashoy");
	
	obj.style.display="block";
	objhorasentrega.style.display="block";
	obj2.style.display="none";
	objhoras.style.display="none";
	
	f=document.form1;
	//colocar los montos visibles
	var obj11=document.getElementById("divcosenvexp");
	obj11.innerHTML=f.txtscosenvexp.value;
	//calcular el total
	calculartotaladicionales("0");
  }

  function procesarpagotc(codvta)
  {
    f=document.form1;
	f.txtoperacion.value="pago tc app";
	f.txtcodvta.value=codvta;
    iniciarpagotc(codvta,f.txtoperacion.value);
  }
  
 
 
 var objIAB;
 function iniciarpagotc(codvta,operacion)
 {
   var na=cadenaaleatoria(30);
   //mostrariframepagotc();
   var iframe=document.getElementById("ifrpago");
   //var p=urlservidor("saciregistertopay.php");
   //var p=urlservidor("saciframe.html");
   f=document.form1;
   var codale=f.txtcodale.value;
   var logusu=f.txtlogusu.value;
   f.txtcodvtaant.value=codvta;
   //var p="saciframe.html";
   var p=urlservidor("saciregistertopay.php");
   p=p+"?txtn="+na+"&txtv="+codvta+"&txto="+operacion+"&txta="+codale+"&txte="+logusu;
   
   
   objIAB=cordova.InAppBrowser.open(p,"_blank","location=no"); //es el que funciona
   //window.open(p,"_blank","location=no");
   //objIAB=window.open(p,"_blank","location=yes");
   
   
   //el evento loadstart era el que funcionaba adecuadamente para lo necesitado
   /*objIAB.addEventListener('loadstop', function(event)
   {     
     alert("lstop="+event.url);   
     if (event.url.match("mobile/close/aceptar"))
	 {
       objIAB.close();
	 }
	 else if (event.url.match("mobile/close/modificarcarro"))
	 {
	   objIAB.close();
       recargarcarrocompras();
     }
	 
   });*/
   
   objIAB.addEventListener('loadstart', function(event)
   {        
     //alert("lstart="+event.url);   
     if (event.url.match("mobile/close/aceptar"))
	 {
	   cerrarventanamodalvolverpaypal();
	   mostrargaleriatipoproducto();
       objIAB.close();	   
     }
	 else if (event.url.match("mobile/close/modificarcarro"))
	 {
	   cerrarventanamodalvolverpaypal();
	   objIAB.close();
       recargarcarrocompras();
     }
	 else if (event.url.match("mobile/close/cambiarpago"))
	 {
	   cerrarventanamodalvolverpaypal();
	   objIAB.close();
       modificarfpventaant();
     }
	 else if (event.url.match("mobile/close/nuevacompra"))
	 {
	   cerrarventanamodalvolverpaypal();
	   objIAB.close();
       anularventapornuevacompra();
     }
   });
   
   
 }
 
 function ciclocirrepago()
 {
   setTimeout("verificarcierrepago()",500);
 }
 
 function verificarcierrepago()
 {
   f=document.form1;
   f.txtoperacion.value="buscar buzon codvta";
   ejecutarbuzon();
 }
 
 function recargarcarrocompras()
 {
   f=document.form1;
   mostrarcargando();
   f.txtoperacion.value="recargar compra codvta app";
   ejecutarrecargarcarroapp();
 }
 
 function anularventapornuevacompra()
 {
   f=document.form1;
   
   
   mostrarcargando();
   f.txtoperacion.value="anular venta por nueva compra app";
   
   ejecutarrecargarcarroapp();
 }
 
 function cambioformapago()
 {
   continuarpago2();
 }
 