  var objfrm=new Array();
  objfrm[0]="txtoperacion";
  objfrm[1]="txtcantraban";
  objfrm[2]="txtmontototal";
  objfrm[3]="txtfulnamenv";
  objfrm[4]="txtcedide";
  objfrm[5]="txttelusuenv";
  objfrm[6]="cmbcodpaienv";
  objfrm[7]="cmbcodestenv";
  objfrm[8]="cmbcodciuenv";
  objfrm[9]="cmbcodurbenv";
  objfrm[10]="txtdirusuenv";
  objfrm[11]="txttasiva";
  objfrm[12]="txtcodvta";
  objfrm[13]="txtfecent";
  objfrm[14]="txthorent";
  objfrm[15]="txtlogusu";
  objfrm[16]="txtcodale";
  objfrm[17]="txtmontoapagar";
  objfrm[18]="txtmondocori";
  objfrm[19]="txtcodbonasi";
  objfrm[20]="txtmonbon";
  objfrm[21]="txttipmon";
  objfrm[22]="txtcottipmon";
  objfrm[23]="txtstaenvexp";
  objfrm[24]="txtcosenvexp";
  objfrm[25]="txtmonmincom";
  objfrm[26]="cmbformapago";
  objfrm[27]="txtmonpagefe";
  
  
  var buscarxml=new Array();
  buscarxml[0]="operacion";
  buscarxml[1]="cantraban";
  buscarxml[2]="montototal";
  buscarxml[3]="fulnamenv";
  buscarxml[4]="cedide";
  buscarxml[5]="telusuenv";
  buscarxml[6]="codpaienv";
  buscarxml[7]="codestenv";
  buscarxml[8]="codciuenv";
  buscarxml[9]="codurbenv";
  buscarxml[10]="dirusuenv";
  buscarxml[11]="tasiva";
  buscarxml[12]="codvta";
  buscarxml[13]="fecent";
  buscarxml[14]="horent";
  buscarxml[15]="logusu";
  buscarxml[16]="codale";
  buscarxml[17]="montoapagar";
  buscarxml[18]="mondocori";
  buscarxml[19]="codbonasi";
  buscarxml[20]="monbon";
  buscarxml[21]="tipmon";
  buscarxml[22]="cottipmon";
  buscarxml[23]="staenvexp";
  buscarxml[24]="cosenvexp";
  buscarxml[25]="monmincom";
  buscarxml[26]="codvta"; //formapago no se graba en la cabecera. Se repite un campo porque no viene en el XML
  buscarxml[27]="codvta"; //monpagefe no se graba en la cabecera. Se repite un campo porque no viene en el XML
  
  var ncampos=28;
 
  function ejecutar()
  {
    var grid=paraenviargrid();
	
    iralservidor2(urlservidor("sicnventaweb.php"),objfrm,ncampos,grid);	
  } 

  function ejecutaracceso()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtpasusumd5";
	obj[3]="txtcodale";
	obj[4]="txtFCMId";
	iralservidor(urlservidor("svcnclientes.php"),obj,5);
  }
  
  function ejecutaringresar()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodale";
	obj[2]="txtverproapp";
	obj[3]="txtFCMId";
	
	iralservidor(urlservidor("svcnclientes.php"),obj,4);
  }
  
  function ejecutarcodsuc()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodsuc";
	obj[2]="txtcodale";
	iralservidor(urlservidorprincipal("svcnsucursales.php"),obj,3);
  }
  
  function ejecutarolvidocontrasena()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusuolvido";
	iralservidor(urlservidor("svcnclientes.php"),obj,2);
  }

  function ejecutarbuzon()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodvta";
	iralservidor(urlservidor("svcnbuzonventasapp.php"),obj,2);
  }
  
  function ejecutarvermicuenta()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	obj[3]="txtverproapp";
	
	iralservidor(urlservidor("svcnclientes.php"),obj,4);
  }
  
  function ejecutarmiscompras()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	
	
	iralservidor(urlservidor("sicnventaweb.php"),obj,3);
  }
  
  function ejecutarvervta()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	obj[3]="txtcodvta";
	
	iralservidor(urlservidor("sicnventaweb.php"),obj,4);
  }
  
  function ejecutarverinfocms()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	obj[3]="txtcodfotnot";
	iralservidor(urlservidor("svcnclientes.php"),obj,4);
  }
  
  
  function ejecutarcerrarsesion()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	
	iralservidor(urlservidor("svcnclientes.php"),obj,3);
  }
  

  function ejecutarregistrate()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	iralservidor(urlservidor("svcnclientes.php"),obj,3);
  }

  function ejecutarguardarregistrate()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtemacli";
	obj[2]="txtcodale";
	obj[3]="txtnomcli";
	obj[4]="cmbprerifcli";
	obj[5]="txtrifcli";
	obj[6]="cmbregcodpai";
	obj[7]="cmbregcodest";
	obj[8]="txttelcli";
	obj[9]="txtdircli";
	obj[10]="txtpascli";	
	obj[11]="txtregcupref";
	obj[12]="txtfecnac";
	obj[13]="txtFCMId";
	
	iralservidor(urlservidor("svcnclientes.php"),obj,14);
  }  
  
  function ejecutarcargargaleria()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtfiltrocodtippro";
	obj[2]="txtndesreg";
	obj[3]="txtlogusu";
	obj[4]="txtcodale";
	obj[5]="txtfiltrocodtipprocat";
	obj[6]="txtbuscar";
	obj[7]="txttipmon";
	obj[8]="txtverproapp";
	f=document.form1;
	f.txtfiltrocodtipprocat.value="";
	//alert(f.txtfiltrocodtipprocat.value);
	iralservidor(urlservidor("svcnproductossimples.php"),obj,9);
	
  }
  
  function ejecutarrecargarcarroapp()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodvtaant";
	obj[2]="txtlogusu";
	obj[3]="txtcodale";	
	obj[4]="txttipmon";	
	obj[5]="txtverproapp";
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,6);
	
  }
  
  function ejecutaragregarcarro()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodprd";
	obj[2]="txtcantidadagregar";
	obj[3]="txtlogusu";
	obj[4]="txtcodale";	
	obj[5]="txtcodcar";	
	obj[6]="txtcodmed";	
	obj[7]="txtdcp";	
	obj[8]="txttipmon";	
	obj[9]="txtverproapp";
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,10);
	
  }
  
  function ejecutareliminarcarro()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodprd";
	obj[2]="txtlogusu";
	obj[3]="txtcodale";	
	obj[4]="txtcodcar";	
	obj[5]="txtcodmed";	
	obj[6]="txtdcp";	
	//obj[7]="txtcantidadagregar";
	obj[7]="txttipmon";	
	obj[8]="txtverproapp";
	f=document.form1;
	//alert("ff="+f.txtcantidadagregar.value);
    iralservidor(urlservidor("svcnproductossimples.php"),obj,9);
	
  }
  
  function ejecutarvercodprd()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodprd";
	obj[2]="txtcantidadagregar";
	obj[3]="txtlogusu";
	obj[4]="txtcodale";	
	obj[5]="txtcodcar";	
	obj[6]="txtcodmed";
    obj[7]="txttipmon";
    obj[8]="txtverproapp";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,9);
	
  }
  
  function ejecutarvercarrito()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	obj[3]="txtverproapp";	
	obj[4]="txttipmon";	
	iralservidor(urlservidor("svcnproductossimples.php"),obj,5);
	
  }
  
  
  function ejecutarpagarcarro()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	obj[3]="txttipmon";	
	obj[4]="txtverproapp";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,5);
	
  }
  
  function ejecutarcambiartipomoneda()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,3);
	
  }
  
  function ejecutarcodcup()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	
    iralservidor(urlservidor("sicnbonosdescuento.php"),obj,3);
	
  }
  
  function ejecutarcodbon()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";
	iralservidor(urlservidor("svcnclientes.php"),obj,3);
  }
  
  function ejecutarvernotificaciones()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,3);
	
  }
  
  function ejecutarvermibalance()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,3);
    
	
  }
  
  
  
  function ejecutarverdirenv()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,3);
	
  }
  
  function ejecutarcoddirenv()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	obj[3]="txtcoddirenv";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,4);
	
  }
  
  
  function ejecutarvernotificacion()
  {
    var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtlogusu";
	obj[2]="txtcodale";	
	obj[3]="txtcodnot";	
	
    iralservidor(urlservidor("svcnproductossimples.php"),obj,4);
	
  }
  
  function ejecutarcodtippro()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txttipmon";
	obj[2]="txtverproapp";
	obj[3]="txtlogusu";
	obj[4]="txtcodale";	
	
	iralservidor(urlservidor("svcncategoriasproducto.php"),obj,5);
	
  }
  
  function ejecutarcargarcodmarfunpro()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	
	iralservidor(urlservidor("svcnmarcasfuncionalesproductos.php"),obj,1);
  }
  
  function ejecutarcodmarfunpro()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodmarfunproaux";
	
    iralservidor(urlservidor("svcnmodelosfuncionalesproductos.php"),obj,2);
  }
  
  function ejecutarcodciuenv()
  {
	var obj=new Array();
	obj[0]="txtoperacion";
	obj[1]="txtcodciuenv";	
	
	iralservidor(urlservidor("svcnurbanizacionesventas.php"),obj,2);
    
  }
  
  function respuestaServidor()
   {
     f=document.form1;
	 if (http_request.readyState == 4)
	 {
	   if (http_request.status == 200)
	   {
	     //alert(http_request.responseText);
		 f.txterror.value=http_request.responseText;
			
	     if (http_request.responseText.indexOf('@@@incorrecto@@@') == -1)
		 { 
           var xmlDocument = http_request.responseXML; 
		   
		   var operacion = xmlDocument.getElementsByTagName('operacion').item(0).firstChild.data;
		   if (operacion=="inicio app 2")// agregado por las sucursales. La sesion se debe crear en la sucursal 21072020
		   {
			 f=document.form1; 
			 var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).firstChild.data;
			 if (encontrado=="si")
			 {			   
			   var na=cadenaaleatoria(15);			   
			   var logusu = xmlDocument.getElementsByTagName('logusu').item(0).firstChild.data;
			   var codale = xmlDocument.getElementsByTagName('codale').item(0).firstChild.data;
			   f.txtcodale.value=codale;
			   window.localStorage.setItem("lscodale", codale);
			   logusu=limpiarvacios(logusu);
			   cargadoconsucursal();
			   
			 }
			 else
			 {
			   alert("Error al tratar de ingresar. Verifique su conexi�n a internet");
			 }
		   }
		   else if (operacion=="buscar accesoapp")
		   {
			 f=document.form1; 
			 var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).firstChild.data;
			 if (encontrado=="si")
			 {			   
			   var na=cadenaaleatoria(15);			   
			   var logusu = xmlDocument.getElementsByTagName('emacli').item(0).firstChild.data;
			   var codale = xmlDocument.getElementsByTagName('codale').item(0).firstChild.data;
			   
			   window.localStorage.setItem("lslogusu", f.txtlogusu.value);
			   //window.localStorage.setItem("lspasusu", f.txtpasusu.value);
			   window.localStorage.setItem("lspasusu", f.txtpasusumd5.value); //02042020 guardando el password en md5
			   window.localStorage.setItem("lscodale", f.txtcodale.value);
			   window.localStorage.setItem("lsstamd5", "S");
			   
			   //location.href="sacmprincipal.html?txtlogusu="+logusu+"&txtcodale="+codale+"&pagar=si&na="+na;
			   document.getElementById("divcapaiconosnotcar").style.display="block";
			   f.txtoperacionaux.value="cargargaleriacodtippro";
	           vercarrito2(); //08012020 
			   
			   //vercarritoiniciosesion(); //08012020 debe mostrarse el carrito. Se llamo a iniciar sesion desde el carro de compras
			   
			   
			   //mostrarcargando();
			   //f.txtoperacion.value="cargar form registrar pago app";
	           //ejecutarpagarcarro();    
			 }
			 else
			 {
			   //mostrarmodalmensaje("Usuario incorrecto",0);
			   alert("Usuario incorrecto");
			   mostrariniciarsesion();
			 }
		   }
		   
		   else if (operacion=="actualizar fcmide")
		   {
			 f=document.form1; 
			 //alert("FCM actualizado");			 			 
		   }
		   
		   else if (operacion=="buscar buzon codvta")
		   {
			 f=document.form1; 
			 var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).textContent;   
			 encontrado=limpiarvacios(encontrado);
             if (encontrado=="si")
             {
			   var menbuz = xmlDocument.getElementsByTagName('menbuz').item(0).textContent;   
			   menbuz=limpiarvacios(menbuz);
			   if (menbuz=="PA")
			   {
			     objIAB.close();
			     mostrargaleriatipoproducto();
			   }
			   else
			   {
			     ciclocirrepago();
			   }
			 }			 
		   }
		   
		   else if (operacion=="buscar codsuc app")
		   {
			 f=document.form1; 
			 var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).textContent;   
			 encontrado=limpiarvacios(encontrado);
             if (encontrado=="si")
             {
			   var codsuc = xmlDocument.getElementsByTagName('codsuc').item(0).textContent;   
			   codsuc=limpiarvacios(codsuc);
			   var nomsuc = xmlDocument.getElementsByTagName('nomsuc').item(0).textContent;   
			   nomsuc=limpiarvacios(nomsuc);
			   var dirser = xmlDocument.getElementsByTagName('dirser').item(0).textContent;   
			   dirser=limpiarvacios(dirser);
			   var rutcarser = xmlDocument.getElementsByTagName('rutcarser').item(0).textContent;   
			   rutcarser=limpiarvacios(rutcarser);
			   cerrarmodalcargando();
			   selsuc(codsuc,nomsuc,dirser,rutcarser);
			   
			 }			 
		   }
		   
		   else if (operacion=="cargar codtippro app")
		   {
		     f=document.form1;
		     f.txtstagalcodtipro.value="S";
			 var htmlmovil = xmlDocument.getElementsByTagName('htmlcodtippro').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 //var obj=document.getElementById("divcapagaleriatipoproductos");
			 var obj=document.getElementById("divcapagaleriatipoproductoshijo");
	         obj.innerHTML=htmlmovil;
			 var htmlslides = xmlDocument.getElementsByTagName('htmlslides').item(0).textContent;   
			 htmlslides=limpiarhtml(htmlslides);
			 htmlslides=limpiarvacios(htmlslides);
			 var nslidesapp = xmlDocument.getElementsByTagName('nslidesapp').item(0).textContent;   
			 nslidesapp=limpiarvacios(nslidesapp);
			 f.txtnslides.value=nslidesapp;
			 
			 var tipmon = xmlDocument.getElementsByTagName('tipmon').item(0).textContent;   
			 tipmon=limpiarvacios(tipmon);
			 f.txttipmon.value=tipmon;
			 
			 mostrarmontoycantidadcarrito(xmlDocument);
			 
			 //f.txterror.value=htmlslides;
			 refrescarslide(htmlslides);
			 mostrargaleriatipoproducto();
			
		   }
		   
		   else if  ((operacion=="cargar galeria app")||(operacion=="buscar producto app"))
		   {
		     f=document.form1;
		     f.txtstagalcodprd.value="S";
		     
			 var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 
			 mostrarmontoycantidadcarrito(xmlDocument);
			 
			 //para lograr agregar hijos
			 var objdivnodo=document.createElement("div");
			 objdivnodo.className="row";
			 objdivnodo.innerHTML=htmlmovil;
			 
			 
			 var canreggal=xmlDocument.getElementsByTagName('canreggal').item(0).firstChild.data;   
			 var ncanreggal=parseInt(canreggal);
			 var desreg=xmlDocument.getElementsByTagName('desreg').item(0).firstChild.data;   
			 var ndesreg=parseInt(desreg);
			 var regporpag=xmlDocument.getElementsByTagName('regporpag').item(0).firstChild.data;   
			 var nregporpag=parseInt(regporpag);
			 
			 
			 var obj=document.getElementById("divcapagaleriaproductosgal");
			 obj.className="cssfondoescritorioapp";
			 
			 //es necesario averiguar si es el primer nodo de la paginacion. Si lo es es, primero se eleminan todos los hijos. Sino, se agrega un hijo mas
			 if (ndesreg==0)
			 {
			   //es el primer nodo de la galeria de productos. Debemos eliminar todos los nodos previos en la galeria
			   limpiargaleriaprd();
			 }
			 mostrarcapaelementodiv("divcapacargandoscroll","none");
			 obj.appendChild(objdivnodo);
	         //obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
	         
			 var nomtippro=xmlDocument.getElementsByTagName('nomtippro').item(0).firstChild.data;   
			 nomtippro=limpiarvacios(nomtippro);
			 document.getElementById("divtitulosuper").innerHTML=nomtippro;
			 
			 if (operacion=="buscar producto app")
			 {
			   document.getElementById("divtitulosuper").innerHTML="Buscar";
			 }
			 
			 mostrargaleria();
	         
	         
			 
			
			 //pag mvil
			 
			 /*if (document.getElementById("divcapapaginacionmovil"))
	         {
	           var objeli=document.getElementById("divcapapaginacionmovil");
	           objeli.parentNode.removeChild(objeli);
	         }*/
			 
			 var paginacion=crearpaginacionmovil(ncanreggal,ndesreg,nregporpag);
			 
	         /*if (!((paginacion === null) && (typeof paginacion === "object")))
	         {
			 
			   var contenedor=document.getElementById("divcapagaleriaproductospaginacionmovil");
			 
	           contenedor.appendChild(paginacion);
	         }*/
			 //fin pag movil
			 
		   }
		   
		   else if (operacion=="agregar carro app")
		   {
		     cerrarmodalcargando();
		     var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 var codcar = xmlDocument.getElementsByTagName('codcar').item(0).textContent;   
			 var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			 f=document.form1;
			 f.elements["tcc"+dcp].value=codcar;
			 if (exitoso=="si")
			 {
			   var nncanprd=parseInt(canprd);
			   var productos="producto agregado";
			   if (nncanprd>1)
			   {
			     productos="productos agregados";
			   }
			   mostrarmontoycantidadcarrito(xmlDocument);
			   //mostrarmodalmensaje(canprd+" "+productos+" al carrito de compras",4);
			   mostrarmodalmensaje("Su producto fue agregado",4);
			   //mostrar editar
			   var dtc=document.getElementById("dtc"+dcp);
			   dtc.innerHTML=canprd;
			   
			   mostrareditarcantidad(dcp,canprd);
			   
			 }
			 else
			 {
			   //mostrar normal
			    mostrarcapaelementodiv("divcapacargando","none");	
	            mostrarcapaelementodiv("divcapagaleriaproductos","block");
   
			 }
			 //mostrargaleria();
			 //mostrarcarritoapp(xmlDocument);
		   }
		   
		   else if (operacion=="agregar carro app solo")
		   {
		     cerrarmodalcargando();
			 mostrargaleria();
		     var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 var codcar = xmlDocument.getElementsByTagName('codcar').item(0).textContent;   
			 var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			 f=document.form1;
			 f.elements["tcc"+dcp].value=codcar;
			 if (exitoso=="si")
			 {
			   var nncanprd=parseInt(canprd);
			   var productos="producto agregado";
			   if (nncanprd>1)
			   {
			     productos="productos agregados";
			   }
			   
			   var myElement = document.getElementById("pdcp"+dcp);
			   myElement.scrollIntoView();
			 
			   
			   mostrarmontoycantidadcarrito(xmlDocument);
			   //mostrarmodalmensaje(canprd+" "+productos+" al carro de compras",4);
			   mostrarmodalmensaje("Su producto fue agregado",4);
			   //mostrar editar
			   var dtc=document.getElementById("dtc"+dcp);
			   dtc.innerHTML=canprd;
			   
			   mostrareditarcantidad(dcp,canprd);
			   
			 }
			 else
			 {
			   //mostrar normal
			    mostrarcapaelementodiv("divcapacargando","none");	
	            mostrarcapaelementodiv("divcapagaleriaproductos","block");
   
			 }
			
		   }
		   
		   else if (operacion=="modificar carro app solo")
		   {
		     cerrarmodalcargando();
			 mostrargaleria();
		     var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 var codcar = xmlDocument.getElementsByTagName('codcar').item(0).textContent;   
			 var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			 f=document.form1;
			 f.elements["tcc"+dcp].value=codcar;
			 
			 var myElement = document.getElementById("pdcp"+dcp);
			 myElement.scrollIntoView();
			 
             
			 if (exitoso=="si")
			 {
			   var nncanprd=parseInt(canprd);
			   var productos="producto agregado";
			   if (nncanprd>1)
			   {
			     productos="productos agregados";
			   }
			   mostrarmontoycantidadcarrito(xmlDocument);
			   if (nncanprd>0)
			   {
			     mostrarmodalmensaje("Su carrito de compras fue actualizado",1);
			   }
			   else if (nncanprd==0)
			   {
			     mostrarmodalmensaje("Se elimin� un producto de tu carrito",7);
			   }
			   //mostrar editar
			   var dtc=document.getElementById("dtc"+dcp);
			   dtc.innerHTML=canprd;
			   
			   mostrareditarcantidad(dcp,canprd);
			   
			 }
			 else
			 {
			   //mostrar normal
			    mostrarcapaelementodiv("divcapacargando","none");	
	            mostrarcapaelementodiv("divcapagaleriaproductos","block");
   
			 }
			
		   }
		   
		   else if (operacion=="modificar carro app")
		   {
		     cerrarmodalcargando();
			 var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 if (exitoso=="si")
			 {
			   var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			   var dtc=document.getElementById("dtc"+dcp);
			   
			   var myElement = document.getElementById("pdcp"+dcp);
			   myElement.scrollIntoView();
			 
			   
			   dtc.innerHTML=canprd;
			   mostrareditarcantidad(dcp,canprd);
			   mostrarmontoycantidadcarrito(xmlDocument);
			   
			   var nncanprd=parseFloat(canprd);
			   
			   if (nncanprd>0)
			   {
			     mostrarmodalmensaje("Su carrito de compras fue actualizado",1);
			   }
			   else if (nncanprd==0)
			   {
			     mostrarmodalmensaje("Se elimin� un producto de tu carrito",7);
			   }
			 }
			 else
			 {
			   var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   			   
			   mostrareditarcantidad(dcp,canprd); 
			 }			 
		   }
		   
		   else if (operacion=="modificar carrocc app")
		   {
		     var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 cerrarmodalcargando();
			 if (exitoso=="si")
			 {
			   var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			   if (document.getElementById("dtc"+dcp))
			   {
			   var dtc=document.getElementById("dtc"+dcp);
			   dtc.innerHTML=canprd;
			   //mostrareditarcantidad(dcp); //
			   }
			   var divc=document.getElementById("dtccc"+dcp);
			   divc.innerHTML=canprd;
			   mostrarmontoycantidadcarrito(xmlDocument);
			   mostrarcapaelementodiv("divcapacargando","none");
			   //alert("dcpcc"+dcp);
			   var objdcp=document.getElementById("dcpcc"+dcp);
	           var objpdcp=document.getElementById("pdcpcc"+dcp);
	           var objec=document.getElementById("divcapaeditarcantidad");
	           objec.style.display="none";
	           
	           
	           objdcp.style.display="block";
			   
			   
 			   mostrarcarritoapp(xmlDocument);
			   //mostrarmodalmensaje("Su carrito de compras fue actualizado",1);
			   var nncanprd=parseFloat(canprd);
			   if (nncanprd>0)
			   {
			     mostrarmodalmensaje("Su carrito de compras fue actualizado",1);
			   }
			   else if (nncanprd==0)
			   {
			     mostrarmodalmensaje("Se elimin� un producto de tu carrito",7);
			   }
			 }
			 else
			 {
			   //var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   			   
			   //mostrareditarcantidad(dcp); 
			 }			 
		   }
		   
		   else if (operacion=="eliminar carro app")
		   {
		     var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var canprd = xmlDocument.getElementsByTagName('canprd').item(0).textContent;   
			 cerrarmodalcargando();
			 if (exitoso=="si")
			 {
			   var dcp = xmlDocument.getElementsByTagName('dcp').item(0).textContent;   
			   var cba=document.getElementById("cba"+dcp);
			   if (document.getElementById("dcp"+dcp))
			   {
			   var cec=document.getElementById("cec"+dcp);
			   cec.style.display="none";
			   var objdcp=document.getElementById("dcp"+dcp);
			   objdcp.style.display="block";
			   var cba=document.getElementById("cba"+dcp);
			   cba.style.display="block";
			   
			   //mostrareditarcantidad(dcp); //
			   }
			   mostrarmodalmensaje("Se elimin� un producto de tu carrito",7);
			 }
			 //mostrargaleria();
			 
			 mostrarcarritoapp(xmlDocument);
		   }
		   
		   else if (operacion=="cargar saldocredigo app")
		   {
		     //mostrarcodprdapp(xmlDocument);
			 var monsalcre = xmlDocument.getElementsByTagName('monsalcre').item(0).textContent;   
			 var monsalcredol = xmlDocument.getElementsByTagName('monsalcredol').item(0).textContent;   
			 monsalcre=limpiarvacios(monsalcre);
			 monsalcredol=limpiarvacios(monsalcredol);
			 if (monsalcre=="")
			 {
			   monsalcre="0";
			 }
			 if (monsalcredol=="")
			 {
			   monsalcredol="0";
			 }
			 f=document.form1;
			 var tipmon=f.txttipmon.value;
			 var ff=document.frmsaldomgo;
			 if (tipmon=="bss")
			 {
			   ff.txtmonsalcre.value=formatearnumero(monsalcre,2);
			   ff.txtmonsalcre2.value=ff.txtmonsalcre.value;
			   document.getElementById("divtxtsaldocredido").innerHTML="Saldo CrediGO (BsS)";
			 }
			 else if (tipmon=="dol")
			 {
			   ff.txtmonsalcre.value=formatearnumero(monsalcredol,2);
			   ff.txtmonsalcre2.value=ff.txtmonsalcre.value;
			   document.getElementById("divtxtsaldocredido").innerHTML="Saldo CrediGO ($)";
			 }
			 cerrarmodalcargando();
			 mostrarmodalsaldomgo();
		   }
		   
		   else if (operacion=="cargar sucursales app")
		   {
		     mostrarhtmlsucursales(xmlDocument);			 
		   }
		   
		   else if (operacion=="ver mi balance app")
		   {
		     mostrarhtmlmibalance(xmlDocument);			 
		   }
		   
		   else if (operacion=="ver carrito app")
		   {
		     //mostrarcodprdapp(xmlDocument);
			 mostrarcarritoapp(xmlDocument);
		   }
		   
		   else if (operacion=="recargar compra codvta app")
		   {
		     //mostrarcodprdapp(xmlDocument);
			 mostrarcarritoapp(xmlDocument);
		   }
		   
		   else if (operacion=="anular venta por nueva compra app")
		   {
		     mostrargaleriatipoproducto();
		   }
		   
		   else if (operacion=="ver codprd app")
		   {
		     mostrarcodprdapp(xmlDocument);
		   }
		   
		   else if (operacion=="ver carrito app inicio")
		   {
		     mostrarcarritoapp(xmlDocument);
			 mostrarcantidadnotificacionesnovistas(xmlDocument); 
			 
		   }
		   
		   else if (operacion=="cambiar tipo moneda app")
		   {		     
			 mostrarhtmltiposmonedas(xmlDocument);
		   }		   
		   
		   else if (operacion=="ver notificaciones app")
		   {
		     mostrarhtmlnotificaciones(xmlDocument);			 
		   }
		   
		   else if (operacion=="ver mi balance app")
		   {
		     
		     mostrarhtmlmibalance(xmlDocument);			 
		   }
		   
		   else if (operacion=="ver dir env app")
		   {
		     mostrarhtmlcatenvios(xmlDocument);			 
		   }
		   
		   else if (operacion=="buscar coddirenv")
		   {
		     cargarcoddirenv(xmlDocument);
			 
		   }
		   
		   else if (operacion=="ver miscompras app")
		   {
		     mostrarhtmlmiscompras(xmlDocument);
		   }
		   
		   else if (operacion=="ver codvta app")
		   {
		     mostrarhtmlcodvta(xmlDocument);
		   }
		   
		   else if (operacion=="ver notificacion app")
		   {
		     mostrarhtmlnotificacion(xmlDocument);
			 
		   }
		   
		   else if (operacion=="marcar not app")
		   {
		     //mostrarhtmlnotificacion(xmlDocument);
			 var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var codnot = xmlDocument.getElementsByTagName('codnot').item(0).textContent;   
			 if (exitoso=="si")
			 {
			   var objic=document.getElementById("icmf"+codnot).src="imagenes/notfav.png";
               var objhi=document.getElementById("hnmf"+codnot).href="javascript: desmarnotfav('"+codnot+"');";			   
			 }
		   }
		   
		   else if (operacion=="desmarcar not app")
		   {
		     //mostrarhtmlnotificacion(xmlDocument);
			 var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 var codnot = xmlDocument.getElementsByTagName('codnot').item(0).textContent;   
			 if (exitoso=="si")
			 {
			   var objic=document.getElementById("icmf"+codnot).src="imagenes/notnovista.png";
			   var objhi=document.getElementById("hnmf"+codnot).href="javascript: marnotfav('"+codnot+"');";			   
			 }
		   }
		   
		   else if (operacion=="cargar cupones app")
		   {
		     mostrarhtmlcuponesdescuento(xmlDocument);			 
		   }
		   
		   else if (operacion=="ver mi cuenta app")
		   {
		     
			 mostrarhtmlvermicuenta(xmlDocument);			 
			 
		   }
		   
		   else if (operacion=="ver info codfotnot app")
		   {
		     mostrarhtmlinfocms(xmlDocument);			 
		   }
		   
		   else if (operacion=="cerrar sesion app")
		   {
		     //document.getElementById("divcapaiconosnotcar").style.display="none";
			 //mostrariniciarsesion();
			 f=document.form1;
			 f.txtlogusu.value="";
			 f.txtpasusu.value="";
			 
			 window.localStorage.setItem("lslogusu", "");
			 window.localStorage.setItem("lspasusu", "");
			 window.localStorage.setItem("lscodale", "");
			 window.localStorage.setItem("lsstamd5", "");

			 
			 inicializarcarritoapp();
			 document.getElementById("cantnotifi").innerHTML="0";
			 
			 mostrargaleriatipoproducto();		
		   }
		   
		   else if (operacion=="registrate app")
		   {
		     mostrarhtmlregistrate(xmlDocument);			 
		   }
		   
		   else if (operacion=="editar perfil app")
		   {
		     mostrarhtmleditarperfil(xmlDocument);			 
		   }
		   
		   else if (operacion=="guardar registrate app")
		   {
		     var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).textContent;   
			 if (encontrado=="si")
			 {
			   var logusu = xmlDocument.getElementsByTagName('emacli').item(0).firstChild.data;
			   f.txtlogusu.value=logusu;
			   
			   window.localStorage.setItem("lslogusu", f.txtlogusu.value);
			   window.localStorage.setItem("lspasusu", f.txtpascli.value); 
			   window.localStorage.setItem("lscodale", f.txtcodale.value);
			   window.localStorage.setItem("lsstamd5", "S");

			   
			   alert("Registro exitoso");
			   f.txtoperacionaux.value="cargargaleriacodtippro";
			   
			   document.getElementById("divcapaiconosnotcar").style.display="block";
	           //vercarrito2(); //08012020
			   vercarrito();  //08012020
			   
			 }
			 else
			 {
			   var mensaje = xmlDocument.getElementsByTagName('mensajeerror').item(0).textContent;   
			   mensaje=limpiarvacios(mensaje);
			   if (mensaje!="")
			   {
			     alert(mensaje);
			   }
			   else
			   {
			     alert("Error al registrarse. Por favor intente de nuevo.");			 
			   } 
			 }
		   }
		   
		   else if (operacion=="guardar editarperfil app")
		   {
		     var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).textContent;   
			 if (encontrado=="si")
			 {
			   f=document.form1;
			   if (f.txtstapascam.value=="S")
			   {
			     window.localStorage.setItem("lspasusu", f.txtpascli.value); //02042020 guardando el password en md5
			     window.localStorage.setItem("lsstamd5", "S");
			     
			   }
			   alert("Datos guardados");
			   vermicuenta();
			   
			 }
			 else
			 {
			   alert("Error guardando perfil");			 
			 }
		   }
		   
		   else if (operacion=="olvido contrasena app")
		   {
		     var encontrado = xmlDocument.getElementsByTagName('encontrado').item(0).textContent;   
			 if (encontrado=="si")
			 {
			   alert("Te enviamos un email con una nueva contrase�a. En caso de recibir el email en los pr�ximos minutos recuerda revisar tambi�n el buz�n de correo no deseado");
			   mostrariniciarsesion();
			   
			 }
			 else
			 {
			   mostrarolvidocontrasena();
			   alert("No encontramos esta direcci�n email");			 
			   
			 }
		   }
		   
		   else if (operacion=="cargar form registrar pago app")
		   {
		     mostrarhtmlformregistrarpago(xmlDocument);
		   }
		   
		   else if (operacion=="recargar compra codvta app fp")
		   {
		     cargarenviovta(xmlDocument);
		     mostrarhtmlformregistrarpago(xmlDocument);
		   }
		   
		   else if (operacion=="incluir vtaapp")
		   {
		     
		     cerrarmodalcargando();
		     //alert(f.txterror.value);
			 
		     var codvta = xmlDocument.getElementsByTagName('codvta').item(0).textContent;   
			 codvta=limpiarvacios(codvta);
			 
			 
			 var exitoso = xmlDocument.getElementsByTagName('exitoso').item(0).textContent;   
			 exitoso=limpiarvacios(exitoso);
			 
			 
			 
			 var mensajeerror = xmlDocument.getElementsByTagName('mensajeerror').item(0).textContent;   
			 mensajeerror=limpiarvacios(mensajeerror);
			 
			 
			 if (exitoso=="si")
			 {
		
       		   inicializarcarritoapp();
			   
			   var formapago = xmlDocument.getElementsByTagName('formapago').item(0).textContent;   
			   formapago=limpiarvacios(formapago);
			 
			   if (formapago=="PP")
			   {
			     //cargargaleriacodtippro();
				 mostrarventanamodalvolverpaypal(); //ventana por si le dan volver en paypal
				 //mostrargaleriatipoproducto(); //no refrescamos. solo mostramos para evitar que falle la llamada procesarpagotc
			     procesarpagotc(codvta);
			   }
			   else
			   {
			     mostrarmodalmensaje("Su compra fue registrada "+codvta,3);			 
			     cargargaleriacodtippro();
			   }
			 }
			 else if (exitoso=="error")
			 {
		       alert("No se registr� su compra");
			 }
			 else if (exitoso=="no se registro")
			 {
			   if (mensajeerror=="existencia insuficiente de algunos productos")
			   {
			     alert("Su compra no se registr� debido a que la existencia de unos los productos no es suficiente. Su carro de compras fue actualizado.");
				 vercarrito();
			   }
			   else if (mensajeerror=="algunos productos cambiaron de precio")
			   {
			     alert("Su compra no se registr� debido a que algunos productos cambiaron de precio. Su carro de compras fue actualizado.");
				 vercarrito();
			   }
			   else if (mensajeerror=="existencia insuficiente de algunos productos y algunos productos cambiaron de precio")
			   {
			     alert("Su compra no se registr� debido a que algunos productos cambiaron de precio y en otros falta disponiblidad. Su carro de compras fue actualizado.");
				 vercarrito();
			   }
               else
               {
			     alert("Su compra no pudo ser registrada. "+mensajeerror);
				 //alert(mensajeerror);
			   }		       
			 }
		   }
		   
		   else if (operacion=="cambio codciu")
		   {			 
			 cargarcombocodurb(xmlDocument);
		   }
		   
		   else if (operacion=="cargar marcas codmarfunpro")
		   {
		     limpiarcodmarfunpro();
			 var ndet = xmlDocument.getElementsByTagName('ndet').item(0).textContent;   			 
			 ndet=limpiarvacios(ndet);
			 nndet=parseInt(ndet);
			 
			 for (i=0;i<nndet;i++)
			 {
			   var codmarfunpro = xmlDocument.getElementsByTagName('codmarfunpro').item(i).textContent;   			 
			   codmarfunpro=limpiarvacios(codmarfunpro);
			   var nommarfunpro = xmlDocument.getElementsByTagName('nommarfunpro').item(i).textContent;   			 
			   nommarfunpro=limpiarvacios(nommarfunpro);
			   
			   var opcion=document.createElement("option");
               opcion.value=codmarfunpro;
               opcion.text=nommarfunpro;
	           f.cmbcodmarfunpro.options.add(opcion);
	
			 }
			 
			 //cargar combo marcas
			 cargargaleria();
		   }
		   
		   else if (operacion=="cargar codmarfunpro")
		   {
			 f=document.form1;  
			 var nd = xmlDocument.getElementsByTagName('nd').item(0).firstChild.data;   
			 var nnd=parseInt(nd);
			 var i=0;
			 for (i=0;i<nnd;i++)
			 {
			   var codmodfunpro = xmlDocument.getElementsByTagName('codmodfunpro').item(i).firstChild.data;   	 
			   var nommodfunpro = xmlDocument.getElementsByTagName('nommodfunpro').item(i).firstChild.data;   	 
			   codmodfunpro=limpiarvacios(codmodfunpro);
			   nommodfunpro=limpiarvacios(nommodfunpro);
			   var opcion=document.createElement("option");
			   opcion.text=nommodfunpro;
			   opcion.value=codmodfunpro;
			   f.cmbcodmodfunpro.options.add(opcion);
			   
			   
			 }
		   }
		   
           isWorking = false;
        }
		
	    else
	    {
		  cerrarmodalcargando();
          alert('Hay un problema con la solicitud de datos (EG)');
        }
	  }
    }
  }
  
  function inicializarcarritoapp()
  {
    		 var obj=document.getElementById("divcapacarritoapp");
	         obj.innerHTML="";
			 
			 //mostrar monto y cantidad carrito
			 
			 var total="0";
			 var obj=document.getElementById("divtotalmontoapp");
			 
	         obj.innerHTML=formatearnumero(total,2);
			 var cant="0";
			 var obj=document.getElementById("cantcarrito");
	         obj.innerHTML=cant;
			 
			 
			 
  }
  
  function cargarcombocodurb(xmlDocument)
  {
    var ncodurb= xmlDocument.getElementsByTagName('nd').item(0).firstChild.data;	     
			   limpiarcodurbenv();
			   for (i=0;i<ncodurb;i++)
			   {
				 var codurb = xmlDocument.getElementsByTagName("codurb").item(i).firstChild.data;	        
				 var nomurb = xmlDocument.getElementsByTagName("nomurb").item(i).firstChild.data;	        
				 var opcion=document.createElement("option");
				 opcion.value=codurb;
				 opcion.text=nomurb;
				 f.cmbcodurbenv.options.add(opcion);   
			   }
			   if (f.txtcodurbenv.value!="")
			   {
			     f.cmbcodurbenv.value=f.txtcodurbenv.value;
			   }
  }
  
  function cargarenviovta(xmlDocument)
  {
    f=document.form1;
	i=0;
	var data = xmlDocument.getElementsByTagName("codurbenvvta").item(i).firstChild.data;
	f.txtbakcodurbenv.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("fulnamenvvta").item(i).firstChild.data;
	f.txtbakfulnamenv.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("cedidevta").item(i).firstChild.data;
	f.txtbakcedide.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("dirusuenvvta").item(i).firstChild.data;
	f.txtbakdirusuenv.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("telusuenvvta").item(i).firstChild.data;
	f.txtbaktelusuenv.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("codpaienvvta").item(i).firstChild.data;
	f.txtbakcodpaienv.value=limpiarvacios(data);
	
	
	var data = xmlDocument.getElementsByTagName("codestenvvta").item(i).firstChild.data;
	f.txtbakcodestenv.value=limpiarvacios(data);
	
	var data = xmlDocument.getElementsByTagName("codciuenvvta").item(i).firstChild.data;
	f.txtbakcodciuenv.value=limpiarvacios(data);
	
  }
  
  function mostrarcarritoapp(xmlDocument)
  {
    document.getElementById("divcapaiconosnotcar").style.display="block";
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapacarritoapp");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 
			 document.getElementById("divtitulosuper").innerHTML="Carro de compras"; //CARRITO COMPRAS
			 
			 //mostrar monto y cantidad carrito
			 mostrarmontoycantidadcarrito(xmlDocument);
			 var objcantidad=document.getElementById("cantcarrito");
			 var cantidad=objcantidad.innerHTML;
			 if ((cantidad=="0")&&(f.txtiniciosesion.value=="si"))
			 {
			   
			     f.txtoperacionaux.value="";
	             cargargaleriacodtippro();
			   
			 }
			 else
			 {
			 if (f.txtoperacionaux.value=="")
			 {
	           mostrarcarrito();
			 }
             else if (f.txtoperacionaux.value=="cargargaleriacodtippro")
			 {
			   f.txtoperacionaux.value="";
	           cargargaleriacodtippro();
			 }
			 else if (f.txtoperacionaux.value=="pagarcarro")
			 {
			   f.txtoperacionaux.value="";
	           
			   pagarcarro();
			 }
			 } //else cantidad=0
  }
  
  function mostrarmontoycantidadcarrito(xmlDocument)
  {
    var total = xmlDocument.getElementsByTagName('total').item(0).textContent;   
			 total=limpiarvacios(total);
			 var obj=document.getElementById("divtotalmontoapp");
			 obj.innerHTML=formatearnumero(total,2);
			 var cant = xmlDocument.getElementsByTagName('cantidad').item(0).textContent;   
			 cant=limpiarvacios(cant);
			 var obj=document.getElementById("cantcarrito");
	         
			 obj.innerHTML=cant;
  }
  
  function mostrarhtmlinfocms(xmlDocument)
  {
    var subtitfotnot = xmlDocument.getElementsByTagName('subtitfotnot').item(0).textContent;   
			 subtitfotnot=limpiarvacios(subtitfotnot);
			 document.getElementById("divtitulosuper").innerHTML=subtitfotnot;
			 var obj=document.getElementById("divcapainfocms");
			 
	         var htmlmovil = xmlDocument.getElementsByTagName('desfotnot').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 obj.innerHTML=htmlmovil;
			 mostrarinfocms();
  }
  
  function mostrarcodprdapp(xmlDocument)
  {
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapaproductosolo");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 
			 var codcar = xmlDocument.getElementsByTagName('codcar').item(0).textContent;   
			 codcar=limpiarvacios(codcar);
			 var codprd = xmlDocument.getElementsByTagName('codprd').item(0).textContent;   
			 codprd=limpiarvacios(codprd);
			 
			 f.txttipoagregar.value="agregar";
			 if (codcar!="")
			 {
			   //el producto ya esta en el carrito. No lleva boton anadir
			   f.txttipoagregar.value="modificar";
			   
			   vermodalimagenagregardesoloconcodcar(codprd,"",codprd,"modificar","0"); //cantidad se determina al mostrarse	 
			 }
			 
			 mostrarproductosolo();
			 
  }
  
  function mostrarhtmltiposmonedas(xmlDocument)
  {
    f=document.form1;
	cerrarmodalcargando();
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divbotonestipomoneda");
	         obj.innerHTML=htmlmovil;
			 mostrarmodaltipomoneda();
			 //f.txterror.value=htmlmovil;
  }
  
  function mostrarhtmlformregistrarpago(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapaformregistrarpago");
	         obj.innerHTML=htmlmovil;
			 f.txterror.value=htmlmovil;
			 f.txterror2.value=htmlmovil;
			 
			 var total = xmlDocument.getElementsByTagName('total').item(0).textContent;   
			 total=limpiarvacios(total);
			 
			 mostrarmontoycantidadcarrito(xmlDocument);
			 
			 var totalconadicionales = xmlDocument.getElementsByTagName('totalconadicionales').item(0).textContent;   
			 totalconadicionales=limpiarvacios(totalconadicionales);
			 
			 
			 f.txtmontoapagar.value=totalconadicionales;//total;
			 f.txtmontoapagaroriginal.value=totalconadicionales;//total;
			 f.txtmontoapagarsindescuento.value=totalconadicionales;//total;
			 f.txtmondocori.value=totalconadicionales;//total;
			 var nfiltraban = xmlDocument.getElementsByTagName('nfiltraban').item(0).textContent;   
			 nfiltraban=limpiarvacios(nfiltraban);
			 f.txtnfiltraban.value=nfiltraban;
			 
			 var fechahoy = xmlDocument.getElementsByTagName('fechahoy').item(0).textContent;   
			 fechahoy=limpiarvacios(fechahoy);
			 f.txtfechahoy.value=fechahoy;
			 
			 f.txtcottipmon.value=f.txtcarcotmon.value;
			 
			 f.txtdivcapadatpagos.value="";
			 f.txtdivcapacompletardatospago.value="";
			 f.txtdivcapadatosrequeridos.value="";
			 
			 calculartotal();
	         mostrarregistrarpago();
			 cerrarmodalcargando();
			 
			 //recuperar direccion de envio
			 recuperarenvio();
  }
  
  function mostrarhtmlmiscompras(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapamiscompras");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="MIS �RDENES";
			 
			 mostrarmiscompras();
  }
  
  function mostrarhtmlcodvta(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapavercodvta");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="MIS �RDENES";
			 
			 mostrarvercodvta();
  }
  
  function mostrarhtmlnotificaciones(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapanotificaciones");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="NOTIFICACIONES";
			 
			 mostrarcantidadnotificacionesnovistas(xmlDocument);
			 
			 mostrarnotificaciones();
  }
  
  function mostrarhtmlcatenvios(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapadireccionesenvio");
	         obj.innerHTML=htmlmovil;
			 mostrardireccionesenvio();			 
  }
  
  function mostrarhtmlmibalance(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
	htmlmovil=limpiarhtml(htmlmovil);
	htmlmovil=limpiarvacios(htmlmovil);
	var obj=document.getElementById("divcapamibalance");
	obj.innerHTML=htmlmovil;
	mostrarmibalance();			 
  }
  
  function mostrarhtmlsucursales(xmlDocument)
  {
    f=document.form1;
	var cantidad = xmlDocument.getElementsByTagName('cantidad').item(0).textContent;   
	cantidad=limpiarvacios(cantidad);
	var ncantidad=parseInt(cantidad);
	if (ncantidad==1)
	{
	  //solo hay una sucursal activa. Se asume esa sucursal
	}
	else
	{
      var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
	  htmlmovil=limpiarhtml(htmlmovil);
	  htmlmovil=limpiarvacios(htmlmovil);
	  var obj=document.getElementById("divcapahtmlsucursales");
	  obj.innerHTML=htmlmovil;
	  cerrarmodalcargando();
	  mostrarmodalsucursales();			 
	}
  }
  
  function cargarcoddirenv(xmlDocument)
  {
    f=document.form1;
    var direnv = xmlDocument.getElementsByTagName('direnv').item(0).textContent;   
	direnv=limpiarvacios(direnv);
	var cedenv = xmlDocument.getElementsByTagName('cedenv').item(0).textContent;   
	cedenv=limpiarvacios(cedenv);
	var nomenv = xmlDocument.getElementsByTagName('nomenv').item(0).textContent;   
	nomenv=limpiarvacios(nomenv);
	var telenv = xmlDocument.getElementsByTagName('telenv').item(0).textContent;   
	telenv=limpiarvacios(telenv);
	
	
	
	var codciu = xmlDocument.getElementsByTagName('codciu').item(0).textContent;   
	codciu=limpiarvacios(codciu);
	var codpai = xmlDocument.getElementsByTagName('codpai').item(0).textContent;   
	codpai=limpiarvacios(codpai);
	var codest = xmlDocument.getElementsByTagName('codest').item(0).textContent;   
	codest=limpiarvacios(codest);
	
	var codurbcab = xmlDocument.getElementsByTagName('codurbcab').item(0).textContent;   
	codurbcab=limpiarvacios(codurbcab);
	
	f.txtfulnamenv.value=nomenv;
	f.txtcedide.value=cedenv;
	f.txtdirusuenv.value=direnv;
	f.txttelusuenv.value=telenv;
	
	
	
	f.cmbcodpaienv.value=codpai;
	f.cmbcodestenv.value=codest;
	f.cmbcodciuenv.value=codciu;
	
    cargarcombocodurb(xmlDocument);
	document.getElementById("divtitulosuper").innerHTML="Registrar pago";
	f.cmbcodurbenv.value=codurbcab;
	mostrarcapaelementodiv("divcapacargando","none");
	mostrarcapaelementodiv("divcapaformregistrarpago","block");
	mostrarcapaelementodiv("divcapadireccionenvioprecargada","none");	
	mostrarcapaelementodiv("divcapadireccionenvioedicion","block");
	//continuarpago();
	//var elmnt = document.getElementById("cmbcodurbenv");
    //elmnt.scrollIntoView(); 
	
	
  }
  
  function mostrarhtmlcuponesdescuento(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapacuponesdescuento");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="CUPONES";
			 
			 mostrarcuponesdescuento();
			 
  }
  
  function mostrarhtmlvermicuenta(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapamicuenta");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="Men�"; //MI CUENTA
			 
			 var htmlmovil = xmlDocument.getElementsByTagName('html2').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 document.getElementById("divcapacompartirreferido").innerHTML=htmlmovil;
			 
			 
			 mostrarmicuenta();
			 
  }
  
  function mostrarhtmlregistrate(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcaparegistrate");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="Reg�strate";
			 
			 mostrarregistrate();
			 
  }
  
  function mostrarhtmleditarperfil(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapaeditarperfil");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="Editar perfil";
			 
			 mostrareditarperfil();
			 
  }
  
  function actualizarhtmlnotificaciones(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('html').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapanotificaciones");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="NOTIFICACIONES";
			 
			 mostrarcantidadnotificacionesnovistas(xmlDocument);
			 
			 
  }
  
  function mostrarcantidadnotificacionesnovistas(xmlDocument)
  {
    var contnv = xmlDocument.getElementsByTagName('cannotnovistas').item(0).textContent;   
			 var ncontnv=parseInt(contnv);
			 var scontnv=""+ncontnv;
			 if (ncontnv>9)
			 {
			   var scontnv="9+";
			 
			 }
			 document.getElementById("cantnotifi").innerHTML=scontnv;
  }
  
  
  
  function mostrarhtmlnotificacion(xmlDocument)
  {
    f=document.form1;
    var htmlmovil = xmlDocument.getElementsByTagName('mennot').item(0).textContent;   
			 htmlmovil=limpiarhtml(htmlmovil);
			 htmlmovil=limpiarvacios(htmlmovil);
			 var obj=document.getElementById("divcapavernotificacion");
	         obj.innerHTML=htmlmovil;
			 //f.txterror.value=htmlmovil;
			 document.getElementById("divtitulosuper").innerHTML="NOTIFICACIONES";
			 
			 mostrarcantidadnotificacionesnovistas(xmlDocument);
			 actualizarhtmlnotificaciones(xmlDocument);
			 mostrarnotificacion();
  }
  
  function crearpaginacionmoviloriginal(ncanreggal,ndesreg,nregporpag) //lo cambiamos por el scrolldown infinito
  {
	var capapag=null;
	
	if (ncanreggal>nregporpag) //Se debe crear la paginacion
	{	
	  var capapag=document.createElement("div");
	  capapag.id="divcapapaginacionmovil";
	  capapag.className="capagaleriaproductosescritorio";
	  //capapag.textAlign="center";
	  var tabla=document.createElement("table");
	  capapag.appendChild(tabla);
	  //tabla.width="650";
	  //tabla.border="1";
	  tabla.cellSpacing=2;
	  tabla.cellPadding=2;
      var tblbody=document.createElement("tbody");
	
	  var tr = document.createElement("tr");
	  var td = document.createElement("td");  
	  td.innerHTML="<div class='textopaginacion'>Pag:</div>";
	  tr.appendChild(td);
	
	  var i=0;
	  
	  var npagactual=Math.floor(ndesreg/nregporpag)+1;
	  
	  if (npagactual>1)
	  {
		//se debe crear el anterior
		var td=document.createElement("td");
		var td1div = document.createElement("div");
	    td1div.className="enlacemensajeasinpag separacionpag";  
	    var opcion=document.createElement("a");	
        opcion.innerHTML=" "+"<"+" ";
		var ip=npagactual-1;
        opcion.href="javascript: irapaginagaleria("+ip+","+nregporpag+");";
	    td1div.appendChild(opcion);  
	    td.appendChild(td1div);
		tr.appendChild(td);
	  }
	  
	  var npaginasdec=redondeo(ncanreggal/nregporpag,8); //una precision alta para disminuir posibilidad de error
	  var npaginasreal=redondeo((ncanreggal/nregporpag)-Math.floor(ncanreggal/nregporpag),8);
	  var npaginas=1;
	  if (npaginasreal>0)
	  {
		npaginas=redondeo(Math.floor(ncanreggal/nregporpag)+1,0);  
	  }
	  else
	  {
		npaginas=Math.floor(ncanreggal/nregporpag);
	  }
	  if (npaginas>1)//por el if principal de arriba, esto siempre debe ser cierto
	  {
	    var npaginasmostrar=3;
		var npaginas2=npagactual+npaginasmostrar-1;
		var pag1=npagactual;
		if (npaginas2>npaginas)
		{
		  nppp=npaginas2-npaginas;
		  npaginas2=npaginas;
		  pag1=pag1-nppp;
		  if (pag1<1)
		  {
			pag1=1;  
		  }
		}
		//for (i=0;i<npaginas;i++)		
		
		
		for (i=pag1-1;i<npaginas2;i++)
		{
		
		  var ip=i+1;
		  var td = document.createElement("td");  
		  if (npagactual==ip)
		  {
			var td1div = document.createElement("div");
	        td1div.className="enlacemensajeasinpag separacionpag";  
			var opcion=document.createTextNode(" "+ip+" ");
			td1div.appendChild(opcion);
			td.appendChild(td1div);
		  }
		  else
		  {
			var td1div = document.createElement("div");
	        td1div.className="enlacemensajeasinpag separacionpag";  
			var opcion=document.createElement("a");	
            opcion.innerHTML=" "+ip+" ";
            opcion.href="javascript: irapaginagaleria("+ip+","+nregporpag+");";
	        td1div.appendChild(opcion);  
			td.appendChild(td1div);  
		  }
		  tr.appendChild(td);  
		}//for
		
		//hay siguiente
		if (npaginas>npagactual)
	    {
		  var td=document.createElement("td");
		  var td1div = document.createElement("div");
	      td1div.className="enlacemensajeasinpag separacionpag";  
	      var opcion=document.createElement("a");	
          opcion.innerHTML=" "+">"+" ";
		  var ip=npagactual+1;
          opcion.href="javascript: irapaginagaleria("+ip+","+nregporpag+");";
	      td1div.appendChild(opcion);  
	      td.appendChild(td1div);
		  tr.appendChild(td);	
	    }
		
		tabla.appendChild(tr);  
	  }
	}//ncanreggal>nregporpag
	return capapag;
  }
  
  function crearpaginacionmovil(ncanreggal,ndesreg,nregporpag) //paginacion para scrolldown infinito (ya no es con enlaces)
  {
	var capapag=null;
	f.txthaysiggalprd.value="N";
	//alert("ncan="+ncanreggal+" nregporpag="+nregporpag);
	if (ncanreggal>nregporpag) //Se debe crear la paginacion
	{	
	  var capapag=document.createElement("div");
	  capapag.id="divcapapaginacionmovil";
	  capapag.className="capagaleriaproductosescritorio";
	  //capapag.textAlign="center";
	  /*var tabla=document.createElement("table");
	  capapag.appendChild(tabla);*/
	  //tabla.width="650";
	  //tabla.border="1";
	  /*tabla.cellSpacing=2;
	  tabla.cellPadding=2;
      var tblbody=document.createElement("tbody");
	  */
	  /*var tr = document.createElement("tr");
	  var td = document.createElement("td");  
	  td.innerHTML="<div class='textopaginacion'>Pag:</div>";
	  tr.appendChild(td);*/
	
	  var i=0;
	  
	  var npagactual=Math.floor(ndesreg/nregporpag)+1;
	  
	  if (npagactual>1)
	  {
		//se debe crear el anterior
		/*var td=document.createElement("td");
		var td1div = document.createElement("div");
	    td1div.className="enlacemensajeasinpag separacionpag";  
	    var opcion=document.createElement("a");	
        opcion.innerHTML=" "+"<"+" ";
		var ip=npagactual-1;
        opcion.href="javascript: irapaginagaleria("+ip+","+nregporpag+");";
	    td1div.appendChild(opcion);  
	    td.appendChild(td1div);
		tr.appendChild(td);*/
	  }
	  
	  var npaginasdec=redondeo(ncanreggal/nregporpag,8); //una precision alta para disminuir posibilidad de error
	  var npaginasreal=redondeo((ncanreggal/nregporpag)-Math.floor(ncanreggal/nregporpag),8);
	  var npaginas=1;
	  if (npaginasreal>0)
	  {
		npaginas=redondeo(Math.floor(ncanreggal/nregporpag)+1,0);  
	  }
	  else
	  {
		npaginas=Math.floor(ncanreggal/nregporpag);
	  }
	  if (npaginas>1)//por el if principal de arriba, esto siempre debe ser cierto
	  {
	    var npaginasmostrar=3;
		var npaginas2=npagactual+npaginasmostrar-1;
		var pag1=npagactual;
		if (npaginas2>npaginas)
		{
		  nppp=npaginas2-npaginas;
		  npaginas2=npaginas;
		  pag1=pag1-nppp;
		  if (pag1<1)
		  {
			pag1=1;  
		  }
		}
		
		f=document.form1;
		/*for (i=pag1-1;i<npaginas2;i++)
		{
		  
		  var ip=i+1;
		  var td = document.createElement("td");  
		  if (npagactual==(ip-1)) // es la pagina siguiente
		  {
		    
			var td1div = document.createElement("div");
	        td1div.className="enlacemensajeasinpag separacionpag";  
			var opcion=document.createElement("a");	
            opcion.innerHTML=" "+ip+" ";
            opcion.href="javascript: irapaginagaleria("+ip+","+nregporpag+");";
	        td1div.appendChild(opcion);  
			td.appendChild(td1div);  
		  }
		  tr.appendChild(td);  
		}*///for
		
		//hay siguiente
		
		if (npaginas>npagactual)
	    {
		  var ip=npagactual+1;
		  
		  f.txtsiggalprd.value=ip;
		  f.txtsignregporpag.value=nregporpag;          
		  f.txthaysiggalprd.value="S";
	    }
		else
		{
		  
		}
		
		//tabla.appendChild(tr);  
	  }
	}//ncanreggal>nregporpag
	//return capapag;
  }
  
  function irapaginagaleria(ip,nregporpag)
  {
	var nipp=ip;
	nipp--;
	var ndesde=redondeo(nipp*nregporpag,0);
	buscarpaginagaleriaproductos(ndesde);
  }
  
  function irapaginagaleriainf()
  {
    
    f=document.form1;
	var ip=parseInt(f.txtsiggalprd.value);
	var nregporpag=parseInt(f.txtsignregporpag.value);
	var nipp=ip;
	nipp--;
	var ndesde=redondeo(nipp*nregporpag,0);
	
	buscarpaginagaleriaproductos(ndesde);
  }
  
  function buscarpaginagaleriaproductos(ndesreg)
  {
    f=document.form1;
	//f.txtoperacion.value="buscar";
	f.txtoperacion.value="cargar galeria app";
	f.txtndesreg.value=ndesreg;
	
	
	//mostrarcargandogaleria();
	ejecutarcargargaleria();
	//ejecutarcargargaleria();
	//f.submit();
  }
  
  function vermiinteres()
  {
    f=document.form1;
	
	var codmarfunpro=getParameterByName("txtcodmarfunpro");
	var codmodfunpro=getParameterByName("txtcodmodfunpro");
	
	f.txtcodmodfunpro.value=codmodfunpro;
	f.txtcodmarfunpro.value=codmarfunpro;
	
	f.txtoperacion.value="cargar galaria app";
    ejecutarcargargaleria();
  }
  
  function vertodos()
  {
    f=document.form1;
	f.txtcodmodfunpro.value="";
	f.txtcodmarfunpro.value="";
	
	/*f.txtoperacion.value="cargar galaria app";
    ejecutarcargargaleria();*/
	cargargaleria();
  }
  
  function limpiarcodtippro()
  {
	if (f.cmbcodtippro.options)//limpiando el combo
	{
	  i=0;
      for (m=f.cmbcodtippro.options.length;m>0;m--)
         f.cmbcodtippro.remove(i);
	}
	
	var todos="Todos";
	var opcion=document.createElement("option");
    opcion.value="-";
    opcion.text=todos;
	f.cmbcodtippro.options.add(opcion);
  }

  function limpiarcodmarfunpro()
  {
	if (f.cmbcodmarfunpro.options)//limpiando el combo
	{
	  i=0;
      for (m=f.cmbcodmarfunpro.options.length;m>0;m--)
         f.cmbcodmarfunpro.remove(i);
	}
	
	var todos="Todos";
	var opcion=document.createElement("option");
    opcion.value="-";
    opcion.text=todos;
	f.cmbcodmarfunpro.options.add(opcion);
  }
  
  function limpiarcodmodfunpro()
  {
	if (f.cmbcodmodfunpro.options)//limpiando el combo
	{
	  i=0;
      for (m=f.cmbcodmodfunpro.options.length;m>0;m--)
         f.cmbcodmodfunpro.remove(i);
	}
	
	var todos="Todos";
	var opcion=document.createElement("option");
    opcion.value="-";
    opcion.text=todos;
	f.cmbcodmodfunpro.options.add(opcion);
  }
  
  function cambiocodmarfunpro()
  {
    f=document.form1;
	f.txtoperacion.value="cargar marcas codmarfunpro";
	
	limpiarcodmodfunpro();
	if (f.cmbcodmarfunpro.value!="-")
	{
	  f.txtcodmarfunproaux.value=f.cmbcodmarfunpro.value;
	  f.txtoperacion.value="cargar codmarfunpro";
	  ejecutarcodmarfunpro();
	}
  }

  function cargarcombocodmarfunpro()  
  {
    f=document.form1;
	f.txtoperacion.value="cargar marcas codmarfunpro";
	ejecutarcargarcodmarfunpro();
  }
  
  function cargargaleria()  
  {
    f=document.form1;
	f.txtoperacion.value="cargar galaria app";
	f.txtcodtippro.value=f.cmbcodtippro.value;
	f.txtcodmarfunpro.value=f.cmbcodmarfunpro.value;
	f.txtcodmodfunpro.value=f.cmbcodmodfunpro.value;
	ejecutarcargargaleria();
  }

  function limpiargaleriaprd()
  {
    var obj=document.getElementById("divcapagaleriaproductosgal");
	while (obj.hasChildNodes())
	{
	  obj.removeChild(obj.firstChild);
	}
	//alert("Hijos eliminados");
  }  
